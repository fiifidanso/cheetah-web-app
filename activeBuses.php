<?php 

$currentPage = "activeBuses";
require_once "layout/header.php";
require_once "layout/sidebar.php";

$school = new Cheetah\Models\SchoolModel;
$allSchools = $school->getActiveSchoolQuery();


?>
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Active Buses</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Active Buses</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
           
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Filter</h5>
                        <form class="form-horizontal" action="app/formProcessor/schoolProcessor.php" method="post">
                            <input autocomplete="false" name="hidden" type="text" style="display:none;">
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="fname" class="text-right control-label col-form-label">Departure Date:</label>
                                    <div class="">
                                        <input autocomplete="false" placeholder="mm/dd/yyyy" type="date" class="form-control" id="depDate" name="schoolName" >
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="fname" class="text-right control-label col-form-label">School:</label>
                                    <div class="">
                                        <select class="form-control" id="schools" >
                                            <option value="all" selected>ALL</option>
                                            <?php 
                                            while ($data = $allSchools->fetch_assoc()) {
                                                echo "<option value='".$data['id']."'>".$data["name"]."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="fname" class="text-right control-label col-form-label">Status:</label>
                                    <div class="">
                                        <select class="form-control" id="busStatus" >
                                            <option value="all">All</option>
                                            <option value="loading">Loading</option>
                                            <option value="full">Full</option>
                                            <option value="paid">Closed</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="fname" class="text-right control-label col-form-label"></label>
                                    <button type="button" class="btn btn-primary" id="searchResult" style="padding-top: 0px;margin-top: 35px;">Search</button>
                                </div>
                            </div>
                        </form>
                       
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title" id="reportHeading">Active Buses</h5>
                        <table id="reportTable" class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Bus Route</th>
                                    <th scope="col">Bus Type</th>
                                    <th scope="col">Departure Date</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Capacity</th>
                                    <th scope="col">Seats Left</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="loadingBuses" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
 aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="loadedBusTitle">Buses </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="editTransportSessionForm" method="POST" action="app/formProcessor/transportSessionEditProcessor.php">
                <div class="modal-footer">
                    <button data-toggle="modal" data-target="#deleteTransportSession"  type="button" class="btn btn-primary">Complete</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                        <table id="loadedBusTable" class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Mobile Number</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Seat Number</th>
                                        <th scope="col">Pickup Point</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                     
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
                
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="deleteTransportSession" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Complete Loaded Bus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            Are you sure you want to change status to completed?     
            <br>
            <i id="deleteName" style="color: #ff294d;">NB:: It can't be reversed</i>
            <form id="deleteTransportForm" method="post" action="app/formProcessor/loadedBusEditProcessor.php">
                <input name="editID" id="editID" type="hidden"/>
                <input type="hidden" name="method" value="delete"/>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" onclick="$('#deleteTransportForm').submit()" >Complete</button>
                </div>
            </div>
        </div>
</div>


<?php 
require_once "layout/footer.php";
?>