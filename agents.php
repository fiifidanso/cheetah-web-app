<?php 

$currentPage = "Agents";
require_once "layout/header.php";
require_once "layout/sidebar.php";
$agent = new \Cheetah\Models\AgentModel;
$destination = new Cheetah\Models\DestinationModel;

?>
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Agents</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Agents</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
           
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">New Agent</h5>
                        <form class="form-horizontal" action="app/formProcessor/agentProcessor.php" method="post">
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Agent Name:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" required id="agentName" name="agentName" placeholder="Agent Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Agent Number:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" required id="agentNumber" name="agentNumber" placeholder="Agent Number">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">All Agents</h5>
                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Phone Number</th>
                                 <th scope="col">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $agent->drawTable() ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
 aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agent</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editschool" class="form-horizontal" action="app/formProcessor/agentEditProcessor.php" method="post">
                    <div class="form-group row">
                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Agent Name:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="editAgentName" name="editAgentName" placeholder="School Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Agent Number:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="editAgentNumber" name="editAgentNumber" placeholder="School Location">
                        </div>
                    </div>
                   
                    <input name="editAgentId" id="editAgentId" type="hidden"/>
                    <input type="hidden" name="method" value="edit"/>
                </form>
                <input type="hidden" id="currentschoolid"/>
              
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="$('#editschool').submit()">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteAgent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete School</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            Are you sure you want to delete  <i id="agentDeleteName" style="color: #ff294d;">school</i>?     
            <form  id="deleteagentform" method="post" action="app/formProcessor/agentEditProcessor.php">
                <input name="deleteId" id="deleteId" type="hidden"/>
                <input type="hidden" name="method" value="delete"/>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" onclick="$('#deleteagentform').submit()">Delete</button>
                </div>
            </div>
        </div>
</div>



<?php 
require_once "layout/footer.php";
?>