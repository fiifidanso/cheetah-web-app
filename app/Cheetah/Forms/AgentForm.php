<?php

namespace Cheetah\Forms;

class AgentForm extends Form
{
    private $_filteredInput;

    function __construct()
    {
        parent::setPage("Agents");
        $result = parent::validateInput(
            [
                "agentName" =>["required"],
                "agentNumber" =>["required", "mobileNumber"],
            ]
        );
        $this->_filteredInput = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    }

    public function saveForm()
    {  
        $newAgent = new \Cheetah\Models\AgentModel;
      
        return $newAgent->save(
            [
                "name" => $this->_filteredInput["agentName"],
                "pin" => "1234",
                "msisdn" => $_POST["agentNumber"],
                "status" => "active",
                "role" => "agent",
                "company" => $_SESSION["cheetah"]["company"],
            ]
        );
    }


}
?>