<?php

namespace Cheetah\Forms;

class DeleteGenericForm extends Form
{
    private $_filteredInput;

    function __construct($page)
    {
        parent::setPage($page);
        $result = parent::validateInput(
            [
                "deleteId" =>["required"]
            ]
        );
        $this->_filteredInput = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    }

    public function deleteForm($tableName)
    {
        \Cheetah\Models\Model::deleteEntry(
            $this->_filteredInput['deleteId'],
            $tableName
        ); 
       
    }


}
?>