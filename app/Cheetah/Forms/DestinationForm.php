<?php

namespace Cheetah\Forms;

class DestinationForm extends Form
{
    private $_filteredInput;

    function __construct()
    {
        parent::setPage("Destination");
        $result = parent::validateInput(
            [
                "destinationName" =>["required"]
            ]
        );
        $this->_filteredInput = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    }

    public function saveForm()
    {
        $newDestination = new \Cheetah\Models\DestinationModel;
        return $newDestination->save(
            [
                "name" => $this->_filteredInput["destinationName"],
                "status" => "active",
                "company" => $_SESSION["cheetah"]["company"]
            ]
        );
    }


}
?>