<?php

namespace Cheetah\Forms;

class EditAgentForm extends Form
{
    private $_filteredInput;
    
    function __construct()
    {
        parent::setPage("Agents");
        $result = parent::validateInput(
            [
                "editAgentId" =>["required"],
                "editAgentName" =>["required"],
                "editAgentNumber" =>["required", "mobileNumber"],
            ]
        );
        $this->_filteredInput = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    }

   
    public function editAgent()
    {
        $editAgent = new \Cheetah\Models\AgentModel;
        return $editAgent->edit(
            [
                "id" =>$this->_filteredInput["editAgentId"],
                "name" => $this->_filteredInput["editAgentName"],
                "msisdn" => $this->_filteredInput["editAgentNumber"],
            ]
        );
    }


}
?>