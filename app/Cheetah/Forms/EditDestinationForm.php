<?php

namespace Cheetah\Forms;

class EditDestinationForm extends Form
{
    private $_filteredInput;

    function __construct()
    {
        parent::setPage("Destination");
        $result = parent::validateInput(
            [
                "editDestinationName" =>["required"],
                "editId" =>["required"]
            ]
        );
        $this->_filteredInput = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    }

    public function editDestination()
    {
        $editDestination = new \Cheetah\Models\DestinationModel;
        return $editDestination->edit(
            [
                "name" => $this->_filteredInput["editDestinationName"],
                "id" =>$this->_filteredInput["editId"]
            ]
        );
    }


}
?>