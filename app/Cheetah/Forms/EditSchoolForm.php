<?php

namespace Cheetah\Forms;

class EditSchoolForm extends Form
{
    private $_filteredInput;
    private $_destinationId = array();

    function __construct()
    {
        parent::setPage("Schools");
        $result = parent::validateInput(
            [
                "editSchoolName" =>["required"],
                "editSchoolLocation" =>["required"],
                "editId" =>["required"]
            ]
        );
        $this->_filteredInput = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    }

    public function editSchool()
    {
        $schoolModel = new \Cheetah\Models\SchoolModel;
        return $schoolModel->edit(
            [
                "id" =>$this->_filteredInput["editId"],
                "name" => $this->_filteredInput["editSchoolName"],
                "location" => $this->_filteredInput["editSchoolLocation"],
            ]
        );
    }

    public function setDestination()
    {

        foreach ($this->_filteredInput as $key => $content) {
            if (is_numeric($key)) {
                array_push($this->_destinationId, $key);
            }
        }
        
        $schoolModel = new \Cheetah\Models\SchoolModel;
        $schoolModel->resetDestination($this->_filteredInput['editId'], $this->_destinationId);
    }

    public function editDestination()
    {
        $editDestination = new \Cheetah\Models\DestinationModel;
        return $editDestination->edit(
            [
                "name" => $this->_filteredInput["editDestinationName"],
                "id" =>$this->_filteredInput["editId"]
            ]
        );
    }


}
?>