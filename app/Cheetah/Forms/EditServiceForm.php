<?php

namespace Cheetah\Forms;

class EditServiceForm extends Form
{
    private $_filteredInput;
    
    function __construct()
    {
        parent::setPage("Service");
        $result = parent::validateInput(
            [
                "editServiceId" =>["required"],
                "editServiceName" =>["required"],
                
            ]
        );
        $this->_filteredInput = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    }

   
    public function editService()
    {
        $editService = new \Cheetah\Models\BusTypeModel;
        return $editService->edit(
            [
                "id" =>$this->_filteredInput["editServiceId"],
                "name" => $this->_filteredInput["editServiceName"],
              
            ]
        );
    }


}
?>