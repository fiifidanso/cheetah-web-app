<?php

namespace Cheetah\Forms;

class EditTransportSessionForm extends Form
{
    private $_filteredInput;

    function __construct()
    {
        parent::setPage("transportSession");
        $result = parent::validateInput(
            [
                "allEditDates" => ["required"],
                "editId" =>["required"],
                "status" =>["required"]
            ]
        );
        $this->_filteredInput = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    }

    public function editSession()
    {   
        $newTransportSession = new \Cheetah\Models\TransportSessionModel;
        $newTransportSession->edit(
            [
               "status" =>  $this->_filteredInput["status"],
                "id" =>$this->_filteredInput["editId"]
                
            ]
        );

        $busForTransportSession = new \Cheetah\Models\TransportBusSessionModel;
        $allBusesForSessions = $busForTransportSession->fetchAllbuses($this->_filteredInput["editId"]);
        
        $incomingBusSessions = \json_decode($this->_filteredInput["selectedBuses"], true);

       
        
        // Used for setting the price for each selected type of bus 
        // Loop through all registered type of buses that would use the route
        foreach ($allBusesForSessions as $index => $busSession) {

            // Loop through the incoming buses that admin has edited
            foreach ($incomingBusSessions as $busId => $amountDetails) {

                // This means nothing was set for the bus in that array
                if ($amountDetails == null) {
                    continue;
                }
                
                // Edit bus details
                if ($busId == $busSession['bus_id']) {
                    $busForTransportSession->edit(
                        [
                            "status" =>  'active',
                            "id" => $busSession["id"],
                            "amount" => ($amountDetails["amount"]*100),
                            "momo_charge" => ($amountDetails["momo_charge"]*100),
                            "nhil" => ($amountDetails["nhil"]*100)
                            
                        ]
                    );

                    // Unset existing buses and incoming buses
                    $incomingBusSessions[$busId] = null;
                    unset($allBusesForSessions[$index]);
                }
            }
        }

        // If buses were pulled out but not found in user input buses are deleted because user has
        // deleted buses
        foreach ($allBusesForSessions as $busSession) {
            $busForTransportSession->edit(
                [
                    "status" =>  'deleted',
                    "id" => $busSession["id"]
                    
                ]
            );
        }

        // If there are still input left in the user bus input this would be saved. Since this
        // would be a new bus added to the route
        foreach ($incomingBusSessions as $busId => $amount) {
            if ($amount == null)
                continue;
            
            $busForTransportSession->save(
                [
                    "transport_session_id" => $this->_filteredInput["editId"],
                    "bus_id" => $busId,
                    "status" =>  "active",
                    "company" => "cheetah",
                    "amount" => ($amount*100)
                ]
            );
           
        }
        
        

        $departureDates = new \Cheetah\Models\DepartureDatesModel;
        $departureDatesForSessions = $departureDates->fetchAllDepartureDates($this->_filteredInput["editId"]);
        
        $incomingDepartureDates = \json_decode($this->_filteredInput["allEditDates"], true);
        
        echo "<pre>";
        print_r($this->_filteredInput);
        print_r($incomingDepartureDates);
        \print_r($departureDatesForSessions);

        #diasbling date editing for now
        
        // foreach ($departureDatesForSessions as $index => $departureDateSession) {

        //     foreach ($incomingDepartureDates as $innerIndex => $date) {
             
        //         if ($date == $departureDateSession['date']) {

        //             $departureDates->edit(
        //                 [
        //                     "status" =>  'active',
        //                     "id" => $departureDateSession["id"]
        //                 ]
        //             );

        //             $incomingDepartureDates[$innerIndex] = null;

        //             unset($departureDatesForSessions[$index]);
        //         }
        //     }
        // }

        // foreach ($departureDatesForSessions as $departureDateSession) {

        //     $departureDates->edit(
        //         [
        //             "status" =>  'deleted',
        //             "id" => $departureDateSession["id"]
                    
        //         ]
        //     );
        // }

        // foreach ($incomingDepartureDates as  $date) {
        //     if ($date == null) {
        //         continue;
        //     }
        //     $departureDates->save(
        //         [
        //             "transport_route_id" => $this->_filteredInput["editId"],
        //             "date" => $date,
        //             "status" =>  "active",
                   
        //         ]
        //     );
           
        // }

        echo "<pre>";
        print_r($departureDatesForSessions);
        print_r($incomingDepartureDates);
        
       
 

        return true;
    }


}
?>