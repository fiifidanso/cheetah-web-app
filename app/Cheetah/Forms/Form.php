<?php

namespace Cheetah\Forms;


class Form
{   
    private $_error_message='';
    private $_page='';

    //min_length
    //max_length
    //required

    protected function setPage($page)
    {
        $this->_page = $page;
    }

    protected function validateInput(Array $allRequirements)
    {
        foreach ($allRequirements as $key => $requirements) {
            if (isset($_POST[$key])) {
                
                foreach ($requirements as $requirement) {
                    if (!$this->passTest($key, $requirement)) {
                        $this->redirectDueToError($key);
                    }
                }

            } else {
                $this->_error_message=$key." is not set.";
                $this->redirectDueToError($key);
            }
        }

        return ["status"=>true];
    }


    private function redirectDueToError($key) 
    {
        \Cheetah\System\Alert::createFormErrorMessage(
            $this->_page,
            $key,
            $this->_error_message,
            $_POST
        );
        header("Location: ".$_SERVER["HTTP_REFERER"]);
        die();
        
    }

    private function passTest($key, $requirement)
    {
        switch ($requirement)
        {
        case 'required':
            if (strlen($_POST[$key])) {
                return true;    
            }
            $this->_error_message=$key." is required.";
            return false;
            break;
        case 'mobileNumber':
            $msisdn = trim($_POST[$key]);
            if (is_numeric($msisdn) and strlen($msisdn) <= 12) {
                if (substr($msisdn, 0, 1) == '0' and strlen($msisdn) == '10') {
                    $_POST[$key]="233".\substr($msisdn, 1);
                    return true;
                } elseif (substr($msisdn, 0, 3) == '233' and strlen($msisdn) == '12') {
                    return true;
                }
            }
            $this->_error_message=$key." should be a valid phone number.";
            return false;
            break;
        }
    }
}
?>