<?php

namespace Cheetah\Forms;

class SchoolForm extends Form
{
    private $_filteredInput;

    function __construct()
    {
        parent::setPage("Schools");
        $result = parent::validateInput(
            [
                "schoolName" =>["required"],
                "schoolLocation" =>["required"]
            ]
        );
        $this->_filteredInput = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    }

    public function saveForm()
    {
        $newSchool = new \Cheetah\Models\SchoolModel;
        return $newSchool->save(
            [
                "name" => $this->_filteredInput["schoolName"],
                "location" => $this->_filteredInput["schoolLocation"],
                "company" => $_SESSION["cheetah"]["company"],
                "status" => "active"
            ]
        );
    }


}
?>