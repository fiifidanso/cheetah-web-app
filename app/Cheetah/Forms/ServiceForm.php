<?php

namespace Cheetah\Forms;

class ServiceForm extends Form
{
    private $_filteredInput;

    function __construct()
    {
        parent::setPage("Service");
        $result = parent::validateInput(
            [
                "serviceName" =>["required"],
            ]
        );
        $this->_filteredInput = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    }

    public function saveForm()
    {  
        $newService = new \Cheetah\Models\BusTypeModel;
      
        return $newService->save(
            [
                
                "name" => $this->_filteredInput["serviceName"],
                "total_seats" => 0,
                "is_back_more" => 0,
                "total_columns" => 0,
                "company" => $_SESSION["cheetah"]["company"],
                "status" => "active"
            ]
        );
    }


}
?>