<?php

namespace Cheetah\Forms;

class TransportSessionForm extends Form
{
    private $_filteredInput;

    function __construct()
    {
        parent::setPage("newSession");
        $result = parent::validateInput(
            [
                "sessionName" =>["required"],
                "schoolSelect" =>["required"],
                "destinationSelect" =>["required"],
                "selectedBuses" =>["required"],
                "departureDates" =>["required"],
            ]
        );
        $this->_filteredInput = filter_input_array(INPUT_POST, FILTER_DEFAULT);
    }

    public function saveForm()
    {   
        $newTransportSession = new \Cheetah\Models\TransportSessionModel;
        $newTransportSession->save(
            [
                "name" => $this->_filteredInput["sessionName"],
                "status" =>  "pending",
                "company" => $_SESSION["cheetah"]["company"],
                "school_id" => $this->_filteredInput["schoolSelect"],
                "destination_id" => $this->_filteredInput["destinationSelect"],
            ]
        );
        $id = $newTransportSession->getLastInput();

        $transportBusSession = new  \Cheetah\Models\TransportBusSessionModel;
        $buses = \json_decode($this->_filteredInput["selectedBuses"], true);
        $departureDates = \json_decode($this->_filteredInput["departureDates"], true);
       
        foreach ($buses as $busId => $amountDetails) {
            if ($amountDetails == null) {
                continue;
            }
           
            $transportBusSession->save(
                [
                    "transport_session_id" => $id,
                    "bus_id" => $busId,
                    "status" =>  "active",
                    "company" => $_SESSION["cheetah"]["company"],
                    "amount" => ($amountDetails["amount"]*100),
                    "momo_charge" => ($amountDetails["momo_charge"]*100),
                    "nhil" => ($amountDetails["nhil"]*100)
                ]
            );
        }

        $departureDate = new \Cheetah\Models\DepartureDatesModel;
        
         // Saving each date and time bus would move
        
        foreach ($departureDates as $date => $pickupTimes) {
            foreach ($pickupTimes as $time) {
                $departureDate->save(
                    [
                        "transport_route_id" => $id,
                        "date" =>$date,
                        "status" => "active",
                        "time" => $time
                    ]
                );
            }
            
        }

        return true;
    }


}
?>