<?php

namespace Cheetah\Models;



class Admin extends Model
{
    private $_admin;

    function __construct()
    {
        parent::setTable('admin');
        parent::setColumns(
            [
                "email" => "string",
                "password" => "string",
                "name" => "string",
                "customer_id" => "string",
                "level" => "string"
            ]
        );    
    }

    
    public function fetch_user($email)
    {
        $result = parent::pullQuerySet(
            [
                ["email", "=","'$email'"]
            ]
        );

        return $result;

    }
   
   
    public function fetch_sender_id($company) 
    {
        $result = parent::pullQuerySet(
            [
                ["level", "=","'admin'"],
                ["company", "=","'$company'"]
            ]
        )->fetch_assoc();

        return $result["sender_id"];

    }
    public function getAllCompanies()
    {
        $result = parent::customQuery(
            'SELECT
            DISTINCT(company) as company,
            ussd_name as `name`,
            is_haulage
            FROM
            `admin`
            WHERE `status` = "active"
            AND `level` = "admin"'
        );

        return $result;
    }

    /**
     * Used to pull information from from loaded row
     * 
     * @param string $columnName The name of the column which information is to be pulled
     * 
     * @return string The string containing the information pulled from the row
     */
    public function pullColumnInfo($columnName)
    {
        return $this->_receipt[$columnName];
    }

    
    
}
?>