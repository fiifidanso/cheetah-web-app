<?php

namespace Cheetah\Models;

use Cheetah\Forms\DestinationForm;


class AgentModel extends Model
{
    function __construct()
    {
        parent::setTable('users');
        parent::setColumns(
            [
                "name" => "string",
                "pin" => "string",
                "msisdn" => "string",
                "status" => "string",
                "role" => "string",
                "company" => "string"
            ]
        );    
    }

    public function drawTable()
    {
        $value = parent::pullQuerySet(
            [
                ["status" ," =", "'active'"],
                ["company" ," =", "'".$_SESSION["cheetah"]["company"]."'"]
            ]
        );

        while ($data = $value->fetch_assoc()) {
            echo "<tr ".
            "onclick=\"fillEditModal(".$data['id'].", '".$data['name']."', '".
             $data['msisdn']."', '".$data['company']."')\">".
            "<td  data-toggle='modal'  data-target='#editAgent'><a href='#'>".$data['name']."</a></td>".
            "<td>".$data['msisdn']."</td>".
             "<td><a onclick='fillDeleteModal(".$data['id'].", \"".$data['name']."\")' ".
            "data-toggle='modal' data-target='#deleteAgent' href='#' style='color: #ff294d'>".
            "<i class='m-r-10 mdi mdi-close danger' data-toggle='tooltip' ".
            "data-placement='top' title='' data-original-title='Delete'></i></a>".
            "</td>".
            "</tr>";
            
        }
    } 

    public function resetDestination($id, $newDestinations)
    {
        parent::deleteOldRelationships(
            'school_destination_table',
            [
                'columnName' => 'school_id',
                'id' => $id
            ]
        );
        foreach ($newDestinations as $destination) {
            parent::insertRelationship(
                'school_destination_table',
                [
                    'columnName' => 'school_id',
                    'id' => $id
                ],
                [
                    'columnName' => 'destination_id',
                    'id' => $destination
                ]
            );
        }
    }
    
    public function getAllDestination($id)
    {

        $customQuery ="SELECT
            dest.id, dest.name
            FROM
                schools sch
            LEFT JOIN school_destination_table schdest on (sch.id = schdest.school_id)
            LEFT JOIN destination dest on (dest.id = schdest.destination_id)
            WHERE
                sch.id = $id
            AND dest.`status`='active'";

        $result = parent::customQuery($customQuery);
        $resultArray = array();
        while ($data= $result->fetch_assoc()) {
            array_push($resultArray, md5($data['id']));
        }
        return $resultArray;
    }
    
    public function pullUSSDDatabase()
    {
        $value = parent::pullQuerySet(
            [["status" ," =", "'active'"]]
        );
        $result = [];
        while ($data = $value->fetch_assoc()) {
            $result[$data["msisdn"]] = [
                "id" => $data["id"],
                "name" => $data["name"],
                "company" => $data["company"]
            ];
        }
        return $result;
    }
}
?>