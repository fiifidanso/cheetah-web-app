<?php

namespace Cheetah\Models;

use Cheetah\Forms\DestinationForm;


class BusTypeModel extends Model
{
    private $_busTypeModel;

    function __construct()
    {
        parent::setTable('bus_types');
        parent::setColumns(
            [
                "name" => "string",
                "total_seats" => "integer",
                "is_back_more" => "integer",
                "total_columns" => "integer",
                "company"=>"string",
                "status" => "string"
            ]
        );
    }

    public function drawTable()
    {
        $value = parent::pullQuerySet(
            [["company","=", "'".$_SESSION["cheetah"]["company"]."'"],
            ["status","=", "'active'"]]
        );

        while ($data = $value->fetch_assoc()) {
            echo '<tr>
                <td>
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" data-bus-id="' . $data['id'] . '" class="custom-control-input bus-type-box" id="' . $data['id'] . '_bus_id">
                        <label class="custom-control-label" for="' . $data['id'] . '_bus_id"></label>
                    </div>
                </td>
                <td>
                    <ul>
                        <li>' . $data['name'] . '</li>
                        <li>' . $data['total_seats'] . ' Seats</li>
                        <li>' . $data['total_columns'] . ' columns</li>
                    </ul>
                </td>
                <td>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label>Ticket Price:</label>
                            <span class="input-group-text" id="basic-addon1">GHS</span>
                            <input type="number" step="0.01" class="form-control bus-amount" id="bus_amount_' . $data['id'] . '"
                            name="bus_amount_' . $data['id'] . '" placeholder="0.00">
                         </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label>Momo Charge:</label>
                            <span class="input-group-text" >GHS</span>
                            <input type="number" step="0.01" class="form-control bus-momo" id="bus_momo_' . $data['id'] . '"
                            name="bus_momo_' . $data['id'] . '" placeholder="0.00">
                         </div>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <label>NHIL:</label>
                            <span class="input-group-text" >GHS</span>
                            <input type="number" step="0.01" class="form-control bus-amount" id="bus_nhil_' . $data['id'] . '"
                            name="bus_nhil_' . $data['id'] . '" placeholder="0.00">
                         </div>
                    </div>
                </td>
            </tr>';

        }
    }

    public function drawHaulageTable()
    {
        $value = parent::pullQuerySet(
            [["company","=", "'".$_SESSION["cheetah"]["company"]."'"],
            ["status","=", "'active'"]]
        );

        while ($data = $value->fetch_assoc()) {
            echo "<tr ".
            "onclick=\"fillEditModal(".$data['id'].", '".$data['name']."')\">".
            "<td  data-toggle='modal'  data-target='#editService'><a href='#'>".$data['name']."</a></td>".
            "<td><a onclick='fillDeleteModal(".$data['id'].", \"".$data['name']."\")' ".
            "data-toggle='modal' data-target='#deleteService' href='#' style='color: #ff294d'>".
            "<i class='m-r-10 mdi mdi-close danger' data-toggle='tooltip' ".
            "data-placement='top' title='' data-original-title='Delete'></i></a>".
            "</td>".
            "</tr>";
            
        }
    } 


    public function getBusTypeModelById($id)
    {
        $this->_busTypeModel = parent::pullQuerySet(
            [
                ["id", "=", $id]
            ]
        )->fetch_assoc();
    }

    /**
     * Used to pull information from from loaded row
     * 
     * @param string $columnName The name of the column which information is to be pulled
     * 
     * @return string The string containing the information pulled from the row
     */
    public function pullColumnInfo($columnName)
    {
        return $this->_busTypeModel[$columnName];
    }
}
?>