<?php

namespace Cheetah\Models;

use Cheetah\Forms\DestinationForm;


class ClientModel extends Model
{
    private $_client;
    
    function __construct()
    {
        parent::setTable('clients');
        parent::setColumns(
            [
                "msisdn" => "string",
                "status" => "string",
                "name" => "string",
                "pin" => "string"
            ]
        );    
    }

  
    
    public function createClient($msisdn)
    {
        $client = parent::pullQuerySet(
            [
                ["msisdn", "=", "'$msisdn'"]
            ]
        );

        if ($client->num_rows) {
            $this->_client = $client->fetch_assoc();
            return;
        }

        parent::save(
            [
                "msisdn" => $msisdn,
                "status" => "active",
                "name" => "",
                "pin" => rand(1000, 9999)
            ]
        );  

        $this->createClient($msisdn);
    }


    public function loadClientByMsisdn($msisdn)
    {

        $msisdn = trim($msisdn);

        if (is_numeric($msisdn) and strlen($msisdn) <= 12) {
            if (substr($msisdn, 0, 1) == '0' and strlen($msisdn) == '10') {
                $msisdn = '233'.substr($msisdn, 1);
            } elseif (substr($msisdn, 0, 3) == '233' and strlen($msisdn) == '12') {
                 $msisdn;
            } else {
                return false;
            }
        } else {
            return false;
        }

        $client = parent::pullQuerySet(
            [
                ["msisdn", "=", "'$msisdn'"]
            ]
        );

        if ($client->num_rows) {
            $this->_client = $client->fetch_assoc();
            return true;
        }

        return false;
    }


    public function loadAllUniqueClientTickets($id)
    {
        $query = "SELECT
        loadBus.id as id,
        tcks.seat_no,
        transSess.name as `name`,
        depDate.date as dep_date,
        bus.total_seats as seats,
        transSess.company as company,
        depDate.time as dep_time
        FROM
            tickets tcks
        LEFT JOIN loading_bus loadBus on (loadBus.id =tcks.loading_bus_id)
        LEFT JOIN transport_session transSess on (transSess.id = loadBus.transport_session)
        LEFT JOIN departure_date depDate on (depDate.id = loadBus.departure_date_id)
        LEFT JOIN bus_types bus on (bus.id = loadBus.bus_type)
        WHERE
            tcks.client_id = $id
        AND loadBus.`status` <> 'completed'
        AND transSess.`status` = 'active'
        ORDER BY loadBus.id";
        
        $availableTickets = [];
        $response = parent::customQuery(($query));

        while ($data = $response->fetch_assoc()) {
            if (!\array_key_exists($data["id"], $availableTickets)) {
                $type = "";
                if ($data['seats'] == '31') {
                    $type = "executive";
                } elseif ($data['seats'] == '49') {
                    $type = "standard";
                }
                $availableTickets[$data["id"]] = [
                    "name" => $data["name"],
                    "departDate" => $data["dep_date"],
                    "busType" => $type,
                    "seatNumbers" => [],
                    "company" => $data["company"],
                    "dep_time" => $data["dep_time"]
                ];

            }

            array_push(
                $availableTickets[$data["id"]]["seatNumbers"],
                $data["seat_no"]
            );

        }
        return $availableTickets;

    }



    public function drawClientInfoTable()
    {

        $msisdn = trim($_GET["msisdn"]);
        
        $extraQuery = '';
        
        if (strlen($msisdn)) {
            if (is_numeric($msisdn) and strlen($msisdn) <= 12) {
                if (substr($msisdn, 0, 1) == '0' and strlen($msisdn) == '10') {
                    $msisdn = '233'.substr($msisdn, 1);

                    $extraQuery = " WHERE cls.msisdn = '$msisdn' ";
                } elseif (substr($msisdn, 0, 3) == '233' and strlen($msisdn) == '12') {
                    $extraQuery = " WHERE cls.msisdn = '$msisdn' ";
                } else {
                    return [];
                }
            } else {
                return [];
            }
        } 
        $company = $_SESSION["cheetah"]["company"];

        $query = "SELECT
            subTable.tck_no,
            subTable.client_id AS client_id,
            subTable.`transport_name` AS transport_name,
            cls.`name` AS client_name,
            cls.`msisdn` AS msisdn,
            cls.`pin` AS pin
        FROM
            clients cls
        INNER JOIN (
            SELECT
                count(tck.id) AS tck_no,
                tck.client_id AS client_id,
                trss.`name` AS transport_name
            FROM
                tickets tck
            LEFT JOIN loading_bus ldbs ON (tck.loading_bus_id = ldbs.id)
            LEFT JOIN transport_session trss ON (
                trss.id = ldbs.transport_session
            )
            WHERE trss.company ='$company'
            GROUP BY
                tck.client_id,
                trss.`name`
        ) subTable ON (cls.id = subTable.client_id)
        $extraQuery
        ORDER BY
            subTable.client_id";

        //echo $query;
        $allClientsInfo = [];
        $response = parent::customQuery(($query));
        $previousClient = '';
        $currentclientInfo = [];

        while ($data = $response->fetch_assoc()) {

            if ($previousClient == $data['client_id']) {
                
                $currentclientInfo["tickets"] .= "<li>".$data["transport_name"]." - <b>(".$data["tck_no"].")</b></li>";
                
            } else {

                if (isset($currentclientInfo["tickets"]) && strlen($currentclientInfo["tickets"])) {
                    array_push(
                        $allClientsInfo,
                        [
                            $currentclientInfo["phoneNumber"],
                            $currentclientInfo["name"],
                            $currentclientInfo["tickets"]."</ul>",
                            $currentclientInfo["pin"]
                        ]
                    );

                    $currentclientInfo = [];
                }
                
                $currentclientInfo["phoneNumber"] = $data["msisdn"];
                $currentclientInfo["name"] = $data["client_name"];
                $currentclientInfo["tickets"] = "<ul><li>".$data["transport_name"]." - <b>(".$data["tck_no"].")</b></li>";
                $currentclientInfo["pin"] = $data["pin"];
                
            }


            $previousClient = $data['client_id'];

        }

        if (isset($currentclientInfo["tickets"]) && strlen($currentclientInfo["tickets"])) {
            array_push(
                $allClientsInfo,
                [
                    $currentclientInfo["phoneNumber"],
                    $currentclientInfo["name"],
                    $currentclientInfo["tickets"]."</ul>",
                    $currentclientInfo["pin"]
                ]
            );

            $currentclientInfo = [];
        }

        return $allClientsInfo;
    }


    /**
     * Used to pull information from from loaded row
     * 
     * @param string $columnName The name of the column which information is to be pulled
     * 
     * @return string The string containing the information pulled from the row
     */
    public function pullColumnInfo($columnName)
    {   
        return $this->_client[$columnName];
    }
}
?>