<?php

namespace Cheetah\Models;


class DepartureDatesModel extends Model
{
    private $_departureDate;

    function __construct()
    {
        parent::setTable('departure_date');
        parent::setColumns(
            [
                "transport_route_id" => "integer",
                "date" => "string",
                "status" => "string",
                "time" => "string"
            ]
        );    
    }
    

    public function drawDates($id)
    {
        $result = parent::pullQuerySet(
            [
                ["transport_route_id", "=", $id],
                ["status", "=", "'active'"]
            ]
        );
        
        if ($result->num_rows) {
            $temp = array();
            $string = "<ul>";
            while ($data= $result->fetch_assoc()) {
                $string .="<li>".$data["date"]." <b>(".$data["time"].")</b></li>";
                array_push($temp, $data["date"]);
            }
            $string .=  "</ul>";

            return [
                "table" => $string,
                "array" => implode('||', $temp)
            ];

        } else {
            return [ 
                "table" => "<ul><li>No dates</li></ul>",
                "array" => ""
            ];
        }

    }

    public function fetchAllDepartureDates($id)
    {
        $dataQuery = $this->pullQuerySet(
            [
                ["transport_route_id" ," =", $id]
            ]
        );

        $result= array();
        while ($data = $dataQuery->fetch_assoc()) {
            array_push($result, $data);
        }

        return $result;
    }

    public function loadById($id)
    {
        $result = parent::pullQuerySet(
            [
                ["id", "=", $id],
               
            ]
        );
        
        $this->_departureDate= $result->fetch_assoc();
    } 

    public function loadUSSDDates($id)
    {
        $dataQuery = parent::customQuery(
            "SELECT * 
            FROM `departure_date`
            WHERE transport_route_id=$id
            AND `status` ='active'
            ORDER BY  `date`, `time` ASC");
        
        // $dataQuery = $this->pullQuerySet(
        //     [
        //         ["transport_route_id" ," =", $id],
        //         ["status", "=", "'active'"]
        //     ]
        // );
           
        $departureDates = [];

        while ($data = $dataQuery->fetch_assoc()) {
            var_dump($data);
            if (array_key_exists($data["date"], $departureDates)) {
                
                array_push(
                    $departureDates[$data["date"]],
                    [
                        "id" => $data["id"],
                        "time" => date("g:i a", strtotime($data["time"]))
                    ]
                );
            } else {
                $departureDates[$data["date"]]=[[
                    "id" => $data["id"],
                    "time" => date("g:i a", strtotime($data["time"]))
                ]];
            }   
            
        }

        return $departureDates;
    }
    /**
     * Used to pull information from from loaded row
     * 
     * @param string $columnName The name of the column which information is to be pulled
     * 
     * @return string The string containing the information pulled from the row
     */
    public function pullColumnInfo($columnName)
    {
        return $this->_departureDate[$columnName];
    }
 
}
?>