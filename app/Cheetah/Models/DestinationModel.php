<?php

namespace Cheetah\Models;

class DestinationModel extends Model
{
    function __construct()
    {
        parent::setTable('destination');
        parent::setColumns(
            [
                "name" => "string",
                "status" => "string",
                "company" => "string"
            ]
        );    
    }

    public function drawTable()
    {
        $value = parent::pullQuerySet(
            [
                ["status" ," =", "'active'"],
                ["company" ," =", "'".$_SESSION["cheetah"]["company"]."'"]
            ]
        );

        while ($data = $value->fetch_assoc()) {
            echo "<tr>".
                "<td><a onclick='fillEditModal(".$data['id'].", \"".$data['name']."\")' data-toggle='modal' ".
                "data-target='#editModal' href='#'>".$data['name']."</a></td>".
                "<td><a onclick='fillDeleteModal(".$data['id'].", \"".$data['name']."\")' ".
                "data-toggle='modal' data-target='#deleteModal' href='#' style='color: #ff294d'>".
                "<i class='m-r-10 mdi mdi-close danger' data-toggle='tooltip' ".
                "data-placement='top' title='' data-original-title='Delete'></i></a></td>".
            "</tr>";
        }
    } 
    
    public function drawCheckBoxes()
    {
        $value = parent::pullQuerySet(
            [
                ["status" ," =", "'active'"],
                ["company" ," =", "'".$_SESSION["cheetah"]["company"]."'"]
            ]
        );

        while ($data = $value->fetch_assoc()) {
            echo ' <div class="custom-control custom-checkbox mr-sm-2">
                <input type="checkbox" value="on" name="'.$data['id'].'" class="custom-control-input" id="'.md5($data['id']).'">
                <label class="custom-control-label" for="'.md5($data['id']).'">'.$data['name'].'</label>
                </div>';
        }
    } 
}
?>