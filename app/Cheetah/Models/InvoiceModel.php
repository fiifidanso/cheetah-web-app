<?php

namespace Cheetah\Models;

class InvoiceModel extends Model
{

    private $_invoice;

    function __construct()
    {
        parent::setTable('invoice');
        parent::setColumns(
            [
                "msisdn" => "string",
                "transport_route_id" => "integer",
                "transport_bus_id" => "integer",
                "number_tickets" => "integer",
                "amount" => "integer",
                "status" => "string",
                "invoice_no" => "string",
                "pay_method" => "string",
                "extra_info" => "string",
                "departure_date_id" => "integer",
                "company" => "string"
            ]
        );
    }


    public function drawReport()
    {
        $parameters = filter_input_array(INPUT_GET, FILTER_DEFAULT);

        $startDate = $parameters['startDate'];
        $endDate = $parameters['endDate'];
        $status = $parameters["status"];

        $query = " WHERE inv.timestamp like '$startDate%'";

        if (strlen($endDate)) {
            $query = " WHERE inv.timestamp >= '$startDate'";

            $query .= " AND inv.timestamp <= '$endDate 23:59:59'";
        }

        if ($status != 'all') {
            $query .= " AND inv.status = '$status'";
        }

        $company = $_SESSION["cheetah"]["company"];

        $query .= " AND inv.company = '$company' ";

        $customQuery = "SELECT
        inv.msisdn,
        trans_sess. NAME AS `name`,
        deptDate.date as departure_date,
        inv.number_tickets,
        inv.amount,
        inv.`status`,
        inv.invoice_no,
        inv.`timestamp`
        FROM
            invoice inv
        LEFT JOIN transport_session trans_sess ON (
            inv.transport_route_id = trans_sess.id
        )
        LEFT JOIN departure_date deptDate ON (
            inv.departure_date_id =deptDate.id
        )
                    $query
                ";

        $result = parent::customQuery($customQuery);
        $returnArray = [];
        while ($data = $result->fetch_assoc()) {

           
            $schedule_date = new \DateTime($data['timestamp'], new \DateTimeZone('EST'));
            $schedule_date->setTimeZone(new \DateTimeZone('UTC'));
            $triggerOn = $schedule_date->format('Y-m-d H:i:s');
            
            array_push(
                $returnArray,
                [
                    $data['msisdn'],
                    $data['name'],
                    $data['departure_date'],
                    $data['number_tickets'],
                    number_format(($data['amount'] / 100), 2),
                    $data['status'],
                    $triggerOn
                ]
            );
        }
        return $returnArray;
    }


    public function loadInvoice($order_id)
    {
        $this->_invoice = parent::pullQuerySet(
            [["invoice_no", " =", "'$order_id'"]]
        )->fetch_assoc();

    }

    public function msisdn()
    {
        return $this->_invoice["msisdn"];
    }

    public function transport_route_id()
    {
        return $this->_invoice["transport_route_id"];
    }

    public function number_tickets()
    {
        return $this->_invoice["number_tickets"];
    }

    /**
     * Used to pull information from from loaded row
     * 
     * @param string $columnName The name of the column which information is to be pulled
     * 
     * @return string The string containing the information pulled from the row
     */
    public function pullColumnInfo($columnName)
    {
        return $this->_invoice[$columnName];
    }
}
?>