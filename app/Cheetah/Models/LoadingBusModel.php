<?php

namespace Cheetah\Models;


class LoadingBusModel extends Model
{
    private $_loadingBus;

    function __construct()
    {
        parent::setTable('loading_bus');
        parent::setColumns(
            [
                "bus_session_id" => "integer",
                "transport_session" => "integer",
                "status" => "string",
                "bus_type" => "integer",
                "empty_seats" => "integer",
                "total_seats" => "integer",
                "departure_date_id" => "integer"
            ]
        );
    }

    public function getCurrentBusLoading($busSessionId, $date_id)
    {
        $result = parent::pullQuerySet(
            [
                ["bus_session_id", "=", "$busSessionId"],
                ["departure_date_id", "=", "$date_id"],
                ["status", "=", "'loading'"]
            ]
        );

        if ($result->num_rows) {
            $this->_loadingBus = $result->fetch_assoc();
            return true;
        }

        return false;
    }


    public function reserveSpace($seatNumber)
    {
        $newSeatNumbers = $this->_loadingBus["empty_seats"] - $seatNumber;

        $update = [
            "id" => $this->_loadingBus["id"],
            "empty_seats" => $newSeatNumbers
        ];

        if ($newSeatNumbers == 0) {
            $update["status"] = "full";
        }
        parent::edit(
            $update
        );
    }

    public function loadByLast()
    {
        $this->_loadingBus = parent::customQuery(
            "SELECT * FROM loading_bus ORDER BY id DESC LIMIT 1"
        )->fetch_assoc();
    }


    public function checkAvailableLoadingBuses($busInRow)
    {
        $result = parent::pullQuerySet(
            [
                ["bus_session_id", "=", $this->_loadingBus["bus_session_id"]],
                ["depature_date", "=", "'" . $this->_loadingBus["depature_date"] . "'"],
                ["status", "=", "'loading'"]
            ]
        );


        for ($i = 1; $i <= $busInRow; $i++) {
            if ($data = $result->fetch_assoc()) {
                echo "$i bus<br>";
                $this->_loadingBus = $data;
            } else {
                break;
            }
        }
       
        //This condition is satisfied when the for loop has succesfully gone through all the 
        //waiting loadine buses

        $numberTimesLoopRun = $i - 1;

        echo "LOOP RUN $numberTimesLoopRun compare to bus to search $busInRow<br><br>";
        if ($numberTimesLoopRun == $busInRow) {
            return true;
        }

        //Returns false when there are no more waiting loading buses in the system
        return false;
    }


    public function switchToMostRecent()
    {

        $result = parent::pullQuerySet(
            [
                ["bus_session_id", "=", $this->_loadingBus["bus_session_id"]],
                ["depature_date", "=", "'" . $this->_loadingBus["depature_date"] . "'"],
                ["status", "=", "'loading'"]
            ]
        );

        //Used to pick the very last row that satisfiest the condition
        while ($data = $result->fetch_assoc()) {
            $this->_loadingBus = $data;
        }

    }


    public function drawReport()
    {
        $parameters = filter_input_array(INPUT_GET, FILTER_DEFAULT);
        
        $depDate = $parameters['depDate'];
        $school =  $parameters['school'];
        $status =  $parameters['status'];
        
        $query = [];

        if ($depDate != '') { 
            array_push($query, "  depDate.date like '$depDate%'");
        }
        
        if ($school != 'all') {
            array_push($query, "  transSess.school_id = $school");
        
        }
        
        if ($status != 'all' ) {
            array_push($query, "  loadBus.status = '$status'");
        }

        $queryString = \implode(" AND ", $query);

        if (strlen($queryString)) {
            $queryString = " AND ".$queryString;
        }
       
        $company = $_SESSION["cheetah"]["company"];

        $customQuery ="SELECT
                loadBus.id,
                transSess.`name` AS busRoute,
                depDate.date AS depature_date,
                loadBus.`status`,
                loadBus.empty_seats,
                loadBus.total_seats,
	            busType.`name` as bus_name
            FROM
                loading_bus loadBus
            LEFT JOIN transport_bus_session transBus ON (
                transBus.id = loadBus.bus_session_id
            )
            LEFT JOIN bus_types busType ON (busType.id = transBus.bus_id)
            LEFT JOIN transport_session transSess ON (
                transSess.id = loadBus.transport_session
            )
            LEFT JOIN departure_date depDate ON (
                loadBus.departure_date_id = depDate.id
            )
            WHERE 
                transSess.`status` <> 'deleted'
            AND transBus.company = '$company'
                $queryString
            ";
        
        $result = parent::customQuery($customQuery);

        $returnArray =[];
        while ($data = $result->fetch_assoc()) {

            if ($data["status"] == "full") {
                $status = '<button type="button" class="btn btn-success">Full</button>';
            } elseif ($data["status"] == "loading") {
                $status = '<button type="button" class="btn btn-warning">Loading</button>';
            } else {
                $status = '<button type="button" class="btn btn-info">Completed</button>';
            }
            
            array_push(
                $returnArray,
                [
                    '<a data-toggle="modal" data-target="#loadingBuses" onclick="loadTable('.$data['id'].', $(this))" href="#">'.$data['busRoute'].'</a>',
                    $data['bus_name'],
                    $data['depature_date'],
                    $status,
                    $data['total_seats'],
                    $data['empty_seats'],
                ]
            );
           
        }
        return $returnArray;
    }


    public function drawTickets()
    {

        $parameters = filter_input_array(INPUT_GET, FILTER_DEFAULT);
        
        $busId = $parameters['busID'];
        

        $customQuery = "SELECT
        clnts.msisdn as `number`,
        clnts.name,
        tckts.seat_no,
	    point.`name` as pickup
        FROM
            loading_bus loadBus
        LEFT JOIN tickets tckts ON ( tckts.loading_bus_id = loadBus.id)
        LEFT JOIN clients clnts ON ( clnts.id = tckts.client_id) 
        LEFT JOIN pickup_points point ON (point.id = tckts.pickup_point_id)
        WHERE
            loadBus.id = $busId";
        
        $result = parent::customQuery($customQuery);
        $returnArray =[];
        while ($data = $result->fetch_assoc()) {
            
            array_push(
                $returnArray,
                [
                    $data["number"],
                    $data["name"],
                    $data["seat_no"],
                    $data["pickup"]
                ]
            );
        }

        return $returnArray;
    }
    
    public function getAllSelectedSeats($id)
    {
        $query = "SELECT
                seat_no
            FROM
                tickets
            WHERE
                loading_bus_id = $id
            AND STATUS = 'active'
            AND seat_no <> 0";

        $result = parent::customQuery($query);
        $seats = [];
        
        while ($data = $result->fetch_assoc()) {
            $seats[$data["seat_no"]] = $data["seat_no"];
        }

        return $seats;
    }
    
    /**
     * Used to pull information from from loaded row
     * 
     * @param string $columnName The name of the column which information is to be pulled
     * 
     * @return string The string containing the information pulled from the row
     */
    public function pullColumnInfo($columnName)
    {
        return $this->_loadingBus[$columnName];
    }
}
?>