<?php

namespace Cheetah\Models;

class MessageModel extends Model
{
    function __construct()
    {
        parent::setTable('messages');
        parent::setColumns(
            [
                "message" => "string",
                "msisdn" => "string",
                "response" => "string",
                "customer_id" => "string",
                "company" => "string"
            ]
        );    
    }



    public function drawReport()
    {
        $parameters = filter_input_array(INPUT_GET, FILTER_DEFAULT);

        $startDate = $parameters['startDate'];
        $endDate = $parameters['endDate'];
       
        $query = " WHERE timestamp like '$startDate%'";

        if (strlen($endDate)) {
            $query = " WHERE timestamp >= '$startDate'";

            $query .= " AND timestamp <= '$endDate 23:59:59'";
        }

       

        $company = $_SESSION["cheetah"]["company"];

        $query .= " AND company = '$company' ";

        $customQuery = "SELECT
            `message`,
            msisdn,
            response,
            `timestamp`
        FROM
            messages
            $query
        ";
        // echo $customQuery;
        $result = parent::customQuery($customQuery);
        $returnArray = [];
        while ($data = $result->fetch_assoc()) {

           
            $schedule_date = new \DateTime($data['timestamp'], new \DateTimeZone('EST'));
            $schedule_date->setTimeZone(new \DateTimeZone('UTC'));
            $formattedDate = $schedule_date->format('Y-m-d H:i:s');
            
            array_push(
                $returnArray,
                [
                    $data['msisdn'],
                    $data['message'],
                    $formattedDate,
                    $data['response'],
                   '<button type="button" class="btn btn-success" onclick="sendthismessage($(this))" style="padding-top: 0px;margin-top: 35px;">'.
                   '<i class="mdi mdi-email m-r-5"></i>Resend</button>'
                   
                ]
            );
        }
        return $returnArray;
    }


}
?>