<?php

namespace Cheetah\Models;


class Model
{
   
    private $_columns;
    private $_table;
    private $_content;

    static function connect()
    {
        $_dbConnection = new \mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
        if ($_dbConnection->connect_errno) {
            echo "Failed to connect to MySQL: " .$_dbConnection->connect_error;
        }
        return $_dbConnection;
    }

    

    protected function setColumns($_columns)
    {
        $this->_columns = $_columns;

    }

    protected function setTable($table)
    {
        $this->_table = $table;
    }

    public function save($content)
    {
        $this->_content = $content;
       
        $query = "INSERT INTO ".$this->_table." ( ". $this->fillColumns()." )".
            " VALUES (".$this->fillValues().")";
        $_dbConnection = $this->connect();
        //echo $query;
        if ($_dbConnection->query($query)) {
            return true;
        } else {
            return false;
        }
            
    }

    private function fillColumns()
    {
        $k = '';
        $i=0;
        foreach ($this->_columns as $key => $column) {

            if ($i) {
                $k.=', '.$key;
            } else {
                $k=$key;
                $i++;   
            }
        }

        return $k;
    } 

    private function fillValues()
    {

        $k = '';
        $i=0;
        foreach ($this->_columns as $key => $column) {

            if ($i) {
                $k.=', '.$this->inputValue($key);
            } else {
                $k=$this->inputValue($key);
                $i++;   
            }
        }

        return $k;
    } 

    private function inputValue($key)
    {
        $formatted = '';
        switch ($this->_columns[$key])
        {
        case 'string':
            $formatted = "'".$this->_content[$key]."'";
            break;
        case 'integer':
            $formatted = $this->_content[$key];
            break;
        }
        return $formatted;
        
    }

    protected function pullQuerySet($options)
    {
        $selectors ='';
        foreach ($options as $option) {
            
            if (strlen($selectors)) {
                $selectors .= " AND ".$option[0]." ".$option[1]." ".$option[2];
            } else {
                $selectors .= " WHERE ".$option[0]." ".$option[1]." ".$option[2];
            }
        }

        $query = "SELECT * FROM ".$this->_table." ".$selectors;
        
        $_dbConnection = $this->connect();
        if ($result = $_dbConnection->query($query)) {
            return $result;
        } else {
            return false;
        }
        
    }

    public  static function deleteEntry($id, $table)
    {
        $query="UPDATE $table SET `status` = 'deleted' where id= $id";
        $_dbConnection = Model::connect();
        if ($_dbConnection->query($query)) {
            return true;
        } else {
            return false;
        }
    }

    public function edit($options)
    {
        $this->_content = $options;
       
        $query = "UPDATE ".$this->_table." SET ".$this->drawUpdateQuery().
            " WHERE id=".$options['id'];
        $_dbConnection = $this->connect();
        if ($_dbConnection->query($query)) {
            return true;
        } else {
            return false;
        }
    }

    private function drawUpdateQuery()
    {

        $k = '';
        $i=0;
        foreach ($this->_columns as $key => $column) {
            if (\array_key_exists($key, $this->_content)) {
                if ($i) {
                    $k.=", $key = ".$this->inputValue($key);
                } else {
                    $k=" $key = ".$this->inputValue($key);
                    $i++;   
                }
            }
        }

        return $k;
    }

    protected function deleteOldRelationships($table, $id)
    {
        $query = "DELETE FROM ".$table." WHERE ".
        " ".$id['columnName']." = ".$id['id'];
        
        $_dbConnection = $this->connect();
        if ($_dbConnection->query($query)) {
            return true;
        } else {
            return false;
        }
    }

    protected  function insertRelationship($table, $id1, $id2)
    {
         $query = "INSERT INTO ".$table." ( ".$id1['columnName'].", ".
            $id2['columnName']." )".
            " VALUES (".$id1['id'].", ".$id2['id'].")";
       
        $_dbConnection = $this->connect();
        if ($_dbConnection->query($query)) {
            return true;
        } else {
            return false;
        }
            
    }


    public static function customQuery($customQuery)
    {
        $_dbConnection = Model::connect();
        if ($result = $_dbConnection->query($customQuery)) {
            return $result;
        } else {
            return false;
        }
        
    }

    protected  function pullRelationship($table, $column)
    {
        $query = "SELECT * FROM ".$table." WHERE ";

        $additonal = "";
        $i = 0;
        foreach ($column as  $value) {
            if ($i) {
                
                $additonal .= " AND ".$value[0]." ".$value[1]." ".$value[2];
   
            } else {
                $i++;
                $additonal .= $value[0]." ".$value[1]." ".$value[2];
            }

        }

        $query .= $additonal;
        
        $_dbConnection = $this->connect();
        
        if ($result = $_dbConnection->query($query)) {
            return $result;
        } else {
            return false;
        }
    }
}

?>