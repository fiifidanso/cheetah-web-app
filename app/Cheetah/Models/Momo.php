<?php

namespace Cheetah\Models;




class Momo extends Model
{
    private $_momo;

    function __construct()
    {
        parent::setTable('momo');
        parent::setColumns(
            [
                "msisdn" => "string",
                "network" => "string",
                "amount" => "string",
                "status" => "string",
                "order_id" => "string",
                "response" => "string",
                "description" => "string",
                "invoice_no" => "string"
            ]
        );
    }

    public function pullOutMomo($orderID)
    {
        $this->_momo = parent::pullQuerySet(
            [
                ["order_id", "=", "'$orderID'"],
                ["status", "=", "'pending'"]
            ]
        );
    }

    public function isPresent()
    {
        return $this->_momo->num_rows;
    }


    public function loadMomo()
    {
        $this->_momo = $this->_momo->fetch_assoc();
    }

    /**
     * Used to pull information from from loaded row
     * 
     * @param string $columnName The name of the column which information is to be pulled
     * 
     * @return string The string containing the information pulled from the row
     */
    public function pullColumnInfo($columnName)
    {
        return $this->_momo[$columnName];
    }

}
?>