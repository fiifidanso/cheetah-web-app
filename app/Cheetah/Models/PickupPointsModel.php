<?php

namespace Cheetah\Models;

class PickupPointsModel extends Model
{
    function __construct()
    {
        parent::setTable('pickup_points');
        parent::setColumns(
            [
                "school_id" => "integer",
                "name" => "string",
                "status" => "string"
            ]
        );    
    }


    public function getAllPoints($id)
    {
        $result = parent::pullQuerySet(
            [
                ["status" ," =", "'active'"],
                ["school_id", "=", $id]
            ]
        );
        
        $resultArray = array();
        while ($data = $result->fetch_assoc()) {
            array_push($resultArray, ["id" => $data['id'], 'name' => $data['name']]);
        }

        return $resultArray;

    }


    public function drawTable()
    {
        $value = parent::pullQuerySet(
            [["status" ," =", "'active'"]]
        );

        while ($data = $value->fetch_assoc()) {
            echo "<tr>".
                "<td><a onclick='fillEditModal(".$data['id'].", \"".$data['name']."\")' data-toggle='modal' ".
                "data-target='#editModal' href='#'>".$data['name']."</a></td>".
                "<td><a onclick='fillDeleteModal(".$data['id'].", \"".$data['name']."\")' ".
                "data-toggle='modal' data-target='#deleteModal' href='#' style='color: #ff294d'>".
                "<i class='m-r-10 mdi mdi-close danger' data-toggle='tooltip' ".
                "data-placement='top' title='' data-original-title='Delete'></i></a></td>".
            "</tr>";
        }
    } 
    
    public function drawCheckBoxes()
    {
        $value = parent::pullQuerySet(
            [["status" ," =", "'active'"]]
        );

        while ($data = $value->fetch_assoc()) {
            echo ' <div class="custom-control custom-checkbox mr-sm-2">
                <input type="checkbox" value="on" name="'.$data['id'].'" class="custom-control-input" id="'.md5($data['id']).'">
                <label class="custom-control-label" for="'.md5($data['id']).'">'.$data['name'].'</label>
                </div>';
        }
    } 
}
?>