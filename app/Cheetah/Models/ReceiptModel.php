<?php

namespace Cheetah\Models;

use Cheetah\Forms\DestinationForm;


class ReceiptModel extends Model
{
    private $_receipt;

    function __construct()
    {
        parent::setTable('receipts');
        parent::setColumns(
            [
                "client_id" => "integer",
                "transport_route_id" => "integer",
                "ticket_quantity" => "integer",
                "amount" => "integer",
                "status" => "string",
                "receipt_no" => "string",
                "pay_method" => "string",
                "invoice_id" => "integer"
            ]
        );    
    }

    public function loadReceiptByReceiptId($receiptNumber)
    {
        $this->_receipt = parent::pullQuerySet(
            [["receipt_no" ," =", "'$receiptNumber'"]]
        )->fetch_assoc();

    }


    public function drawReport()
    {
        $parameters = filter_input_array(INPUT_GET, FILTER_DEFAULT);
        
        $startDate = $parameters['startDate'];
        $endDate =  $parameters['endDate'];
      
        $query = " WHERE rcpts.timestamp like '$startDate%'";
        
        if (strlen($endDate)) {
            $query = " WHERE rcpts.timestamp >= '$startDate'";

            $query .= " AND rcpts.timestamp <= '$endDate 23:59:59'";
        }

        $company = $_SESSION["cheetah"]["company"];

        $query .= " AND trns_sess.company = '$company' ";
         
        $customQuery ="SELECT
                clnts.msisdn,
                trns_sess.`name` as transport_session,
            rcpts.ticket_quantity,
                rcpts.amount,
                rcpts.pay_method,
                users.`name` as agent_name,
            rcpts.`timestamp`
            FROM
                receipts rcpts
            LEFT JOIN clients clnts ON (clnts.id = rcpts.client_id)
            LEFT JOIN transport_session trns_sess ON (trns_sess.id = rcpts.transport_route_id)
            LEFT JOIN user_receipt urs ON (rcpts.id = urs.receipt_id)
            LEFT JOIN users ON (urs.user_id = users.id)
                $query
            ";
        $result = parent::customQuery($customQuery);
        $returnArray =[];
        while ($data = $result->fetch_assoc()) {
            $currentRow = [
                $data['msisdn'],
                $data['transport_session'],
                $data['ticket_quantity'],
                number_format(($data['amount']*0.01), 2)
                
            ];
            if ($data['pay_method'] == "cash" ) {
                array_push($currentRow, "<ul><li>".$data['pay_method']."</li><li>".$data['agent_name']."</li></ul>");
            } else {
                array_push($currentRow, "<ul><li>".$data['pay_method']."</li></ul>");
            }

            $schedule_date = new \DateTime($data['timestamp'], new \DateTimeZone('EST'));
            $schedule_date->setTimeZone(new \DateTimeZone('UTC'));
            $triggerOn = $schedule_date->format('Y-m-d H:i:s');

            array_push($currentRow,  $triggerOn);
            array_push(
                $returnArray,
                $currentRow
            );
        }
        return $returnArray;
    }


    /**
     * Used to pull information from from loaded row
     * 
     * @param string $columnName The name of the column which information is to be pulled
     * 
     * @return string The string containing the information pulled from the row
     */
    public function pullColumnInfo($columnName)
    {
        return $this->_receipt[$columnName];
    }

    
    
}
?>