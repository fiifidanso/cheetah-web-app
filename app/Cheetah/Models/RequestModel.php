<?php

namespace Cheetah\Models;

class RequestModel extends Model
{
    function __construct()
    {
        parent::setTable('messages');
        parent::setColumns(
            [
                "message" => "string",
                "msisdn" => "string",
                "response" => "string",
                "customer_id" => "string",
                "company" => "string"
            ]
        );    
    }



    public function drawReport()
    {
        $parameters = filter_input_array(INPUT_GET, FILTER_DEFAULT);

        $startDate = $parameters['startDate'];
        $endDate = $parameters['endDate'];
       
        $query = " WHERE req.`timestamp` like '$startDate%'";

        if (strlen($endDate)) {
            $query = " WHERE req.`timestamp` >= '$startDate'";

            $query .= " AND req.`timestamp` <= '$endDate 23:59:59'";
        }

       

        $company = $_SESSION["cheetah"]["company"];

        $query .= " AND req.`company` = '$company' ";

        $customQuery = "SELECT
                req.msisdn,
                trnss.`name` as `route`,
                dp.date,
                pp.`name` as pickup,
                req.`status`
            FROM
                request req
            LEFT JOIN transport_session trnss on (req.transport_route_id = trnss.id)
            LEFT JOIN departure_date dp on (req.departure_date_id = dp.id)
            LEFT JOIN pickup_points pp on (req.pickup_point_id = pp.id)
            $query
        ";
        // echo $customQuery;
        $result = parent::customQuery($customQuery);
        $returnArray = [];
        while ($data = $result->fetch_assoc()) {

           
            // $schedule_date = new \DateTime($data['date'], new \DateTimeZone('EST'));
            // $schedule_date->setTimeZone(new \DateTimeZone('UTC'));
            // $formattedDate = $schedule_date->format('Y-m-d');
            
            array_push(
                $returnArray,
                [
                    $data['msisdn'],
                    $data['route'],
                    $data['date'],
                    $data['pickup'],
                   '<button type="button" class="btn btn-success" id="searchResult" style="padding-top: 0px;margin-top: 35px;">'.
                   '<i class="mdi mdi-email m-r-5"></i>pening</button>'
                   
                ]
            );
        }
        return $returnArray;
    }


}
?>