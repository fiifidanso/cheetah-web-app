<?php

namespace Cheetah\Models;

use Cheetah\Forms\DestinationForm;


class SchoolModel extends Model
{
    function __construct()
    {
        parent::setTable('schools');
        parent::setColumns(
            [
                "name" => "string",
                "location" => "string",
                "status" => "string",
                "company" => "string"
            ]
        );    
    }

    public function drawTable()
    {
        $value = parent::pullQuerySet(
            [
                ["status" ," =", "'active'"],
                ["company" ," =", "'".$_SESSION["cheetah"]["company"]."'"]
            ]
        );

        while ($data = $value->fetch_assoc()) {
            echo "<tr ".
            "onclick=\"fillEditModal(".$data['id'].", '".$data['name']."', '".
            $data['location']."', '".implode('||', $this->getAllDestination($data['id']))."')\">".
            "<td data-toggle='modal' data-target='#editSchool' ><a href='#'>".$data['name']."</a></td>".
            "<td>".$data['location']."</td>".
            "<td><a onclick='fillDeleteModal(".$data['id'].", \"".$data['name']."\")' ".
            "data-toggle='modal' data-target='#deleteSchool' href='#' style='color: #ff294d'>".
            "<i class='m-r-10 mdi mdi-close danger' data-toggle='tooltip' ".
            "data-placement='top' title='' data-original-title='Delete'></i></a>".
            "</td>".
            "</tr>";
            
        }
    } 

    public function resetDestination($id, $newDestinations)
    {
        parent::deleteOldRelationships(
            'school_destination_table',
            [
                'columnName' => 'school_id',
                'id' => $id
            ]
        );
        foreach ($newDestinations as $destination) {
            parent::insertRelationship(
                'school_destination_table',
                [
                    'columnName' => 'school_id',
                    'id' => $id
                ],
                [
                    'columnName' => 'destination_id',
                    'id' => $destination
                ]
            );
        }
    }
    
    public function getAllDestination($id)
    {

        $customQuery ="SELECT
            dest.id, dest.name
            FROM
                schools sch
            LEFT JOIN school_destination_table schdest on (sch.id = schdest.school_id)
            LEFT JOIN destination dest on (dest.id = schdest.destination_id)
            WHERE
                sch.id = $id
            AND dest.`status`='active'";

        $result = parent::customQuery($customQuery);
        $resultArray = array();
        while ($data= $result->fetch_assoc()) {
            array_push($resultArray, md5($data['id']));
        }
        return $resultArray;
    }

    public function getAllDestinationUSSDDestination($id)
    {

        $customQuery ="SELECT
            dest.id as id, dest.name as name
            FROM
                schools sch
            LEFT JOIN school_destination_table schdest on (sch.id = schdest.school_id)
            LEFT JOIN destination dest on (dest.id = schdest.destination_id)
            WHERE
                sch.id = $id
            AND dest.`status`='active'";

        $result = parent::customQuery($customQuery);
        $resultArray = array();

        while ($data = $result->fetch_assoc()) {
            array_push(
                $resultArray, 
                [
                "id" =>$data['id'],
                "name" => $data['name']
                ]
            );
        }
       
        return $resultArray;
    }
    
    public function getActiveSchoolQuery()
    {
        $value = parent::pullQuerySet(
            [
                ["status" ," =", "'active'"],
                ["company" ," =", "'".$_SESSION["cheetah"]["company"]."'"]
                ]
        );

        return $value;
    }


    public function getActiveSchoolByCompanyQuery($company)
    {
        $value = parent::pullQuerySet(
            [
                ["status" ," =", "'active'"],
                ["company" ," =", "'".$company."'"]
                ]
        );

        return $value;
    }

    public function getAllSchoolDestinationsQuery($id)
    {

        $customQuery ="SELECT
            dest.id, dest.name
            FROM
                schools sch
            LEFT JOIN school_destination_table schdest on (sch.id = schdest.school_id)
            LEFT JOIN destination dest on (dest.id = schdest.destination_id)
            WHERE
                sch.id = $id
            AND dest.`status`='active'";

        $result = parent::customQuery($customQuery);
        
        return $result;
    }
}
?>