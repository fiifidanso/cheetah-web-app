<?php

namespace Cheetah\Models;


class TicketModel extends Model
{
    private $_ticketMiodel;

    function __construct()
    {
        parent::setTable('tickets');
        parent::setColumns(
            [
                "loading_bus_id" => "integer",
                "receipt_id" => "integer",
                "seat_no" => "integer",
                "status" => "string",
                "pickup_point_id" => "integer",
                "client_id" => "integer",
                 
            ]
        );    
    }


    public function updateSeatSelection($selectedSeats, $busId)
    {

        foreach ($selectedSeats as $key => $seat) {
            if ($seat) {
                parent::customQuery(
                    "UPDATE tickets
                    SET seat_no =$key
                    WHERE
                        loading_bus_id = $busId
                    AND client_id = ".$_SESSION["cheetah"]["clientId"].
                    "
                    AND seat_no = 0
                    LIMIT 1"
                );
                
            }
           
        }
    }

    /**
     * Used to pull information from from loaded row
     * 
     * @param string $columnName The name of the column which information is to be pulled
     * 
     * @return string The string containing the information pulled from the row
     */
    public function pullColumnInfo($columnName)
    {
        return $this->_transportBusSessionModel[$columnName];
    }
 
}
?>