<?php

namespace Cheetah\Models;


class TransportBusSessionModel extends Model
{
    private $_transportBusSessionModel;

    function __construct()
    {
        parent::setTable('transport_bus_session');
        parent::setColumns(
            [
                "transport_session_id" => "integer",
                "bus_id" => "integer",
                "status" => "string",
                "company" => "string",
                "amount" => "integer",
                "momo_charge" => "integer",
                "nhil" => "integer"
               
            ]
        );    
    }


    function getTransportSessionBuses($id) 
    {
        $query = "SELECT
        tbs.id AS id,
        tbs.amount AS amount,
        tbs.momo_charge AS momo_charge,
        tbs.nhil AS nhil,
        bt.name AS `name`,
        bt.total_seats AS seats,
        bt.id as busId
        FROM
            transport_bus_session tbs
        LEFT JOIN bus_types bt ON (bt.id = tbs.bus_id)
        WHERE
            tbs.status = 'active'
        AND tbs.transport_session_id = ".$id;
        
        return parent::customQuery($query);
    }

    function fetchAllbuses($id)
    {
        $dataQuery = $this->pullQuerySet(
            [["transport_session_id" ," =", $id]]
        );

        $result= array();
        while ($data = $dataQuery->fetch_assoc()) {
            array_push($result, $data);
        }

        return $result;
    }

    function fetchAllActivebuses($id)
    {
        $dataQuery = $this->pullQuerySet(
            [
                ["transport_session_id" ," =", $id],
                ["status" ," =", "'active'"]
            ]
        );
      
        $query = "SELECT
            trns_sess.id,
            trns_sess.transport_session_id,
            trns_sess.bus_id,
            trns_sess.status,
            trns_sess.amount,
            trns_sess.momo_charge,
            trns_sess.nhil,
            trns_sess.company,
            bstyp.name
            FROM 
            transport_bus_session as trns_sess
            LEFT JOIN bus_types bstyp on (bstyp.id = trns_sess.bus_id)
            WHERE 
            trns_sess.status = 'active'
            AND trns_sess.transport_session_id=$id
            ";
        $dataQuery = $this->customQuery($query);

        $result= array();
        while ($data = $dataQuery->fetch_assoc()) {
            array_push($result, $data);
        }

        return $result;
    }

    public function loadBusSessionById($id)
    {
        $this->_transportBusSessionModel 
            = parent::pullQuerySet(
                [
                    ["id","=",$id]
                ]
            )->fetch_assoc();
    }

    /**
     * Used to pull information from from loaded row
     * 
     * @param string $columnName The name of the column which information is to be pulled
     * 
     * @return string The string containing the information pulled from the row
     */
    public function pullColumnInfo($columnName)
    {
        return $this->_transportBusSessionModel[$columnName];
    }
 
}
?>