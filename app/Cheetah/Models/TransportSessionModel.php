<?php

namespace Cheetah\Models;


class TransportSessionModel extends Model
{
    private $_transportSessionModel;

    function __construct()
    {
        parent::setTable('transport_session');
        parent::setColumns(
            [
                "name" => "string",
                "status" => "string",
                "company" => "string",
                "school_id" => "integer",
                "destination_id" => "integer"
            ]
        );    
    }

    public function loadRouteById($id)
    {
        $this->_transportSessionModel 
            = parent::pullQuerySet(
                [
                    ["id","=",$id]
                ]
            )->fetch_assoc();
    }

    public function getLastInput()
    {
        $customQuery ="SELECT
                id
            FROM
                transport_session 
            ORDER BY id DESC
            LIMIT 1";

        $result = parent::customQuery($customQuery);
        $resultArray = array();
        $data= $result->fetch_assoc();
        return $data['id'];
    }

    public function drawTransportSessionTable()
    {
        $customQuery ="SELECT
               *
            FROM
                transport_session 
            WHERE 
            status <> 'deleted'
            AND  status <> 'completed'
            AND  company = '".$_SESSION["cheetah"]["company"]."'
            ";
        
        $result = parent::customQuery($customQuery);
        $tansportSessionBuses = new  TransportBusSessionModel;
        $departureDates = new DepartureDatesModel;
           
        while ($data = $result->fetch_assoc()) {
            
            $status = "btn-success";
            if ($data['status'] == 'pending') {
                $status = "btn-warning";
            }
            if ($data['status'] == 'paused') {
                $status = "btn-danger";
            }
            
            $buses = $this->getBusesString($tansportSessionBuses->getTransportSessionBuses($data['id']));
            $dates =  $departureDates->drawDates($data['id']);
            echo "<tr>".
            "<td><a data-toggle='modal' data-target='#editTransportSession' onclick=\"selectSessionRow(".$data['id'].", $(this), '".implode('||', $buses[1])."','".$dates["array"]."')\"  href=\"#\"> ".$data['name']."</a></td>".
            "<td>".$dates["table"]."</td>".
            "<td>".$buses[0]."</td>".
            "<td> <button type=\"button\" class=\"btn ".$status."\">".ucfirst($data['status']).
            "</button></td>".
            "<td><a onclick='fillDeleteModal(".$data['id'].", \"".$data['name']."\")' ".
            "data-toggle='modal' data-target='#deleteTransportSession' href='#' style='color: #ff294d'>".
            "<i class='m-r-10 mdi mdi-close danger' data-toggle='tooltip' ".
            "data-placement='top' title='' data-original-title='Delete'></i></a>".
            "</td>".
            "</tr>";
        }
    }

    private function getBusesString($queryResult)
    {
        $string = '<ul>';

        $selectedBuses = array();
        while ($data = $queryResult->fetch_assoc()) {
            $string .="<li>".$data['name']."<ul>".
            "<li>".$data['seats']." Seats</li>".
            "<li>Ticket: <b>GHS ".number_format(($data['amount']/100), 2)."</b></li>".
            "<li>Momo: <b>GHS ".number_format(($data['momo_charge']/100), 2)."</b></li>".
            "<li>NHIL: <b>GHS ".number_format(($data['nhil']/100), 2)."</b></li></ul></li>";

            array_push(
                $selectedBuses, $data['busId'].'|-|'.number_format(($data['amount']/100), 2).'|-|'.
                number_format(($data['momo_charge']/100), 2).'|-|'.number_format(($data['nhil']/100), 2)
            );
        }

        return [$string."</ul>", $selectedBuses];
    }


    public function getTransportSessionModelUSSD($id) {
        
        $departureDates = new DepartureDatesModel;
       
        $customQuery ="SELECT
        ts.id,
        dt.name,
        ts.destination_id
                FROM
                    transport_session ts
        LEFT JOIN destination dt ON (dt.id = ts.destination_id)
                WHERE
                    ts.school_id = $id
                AND ts.`status` ='active'";
        
        $result = parent::customQuery($customQuery);
        $tansportSessionBuses = new  TransportBusSessionModel;
        
        $resultArray = [];
        while ($data = $result->fetch_assoc()) {
            $tempArray = [];
            
            $tempArray["transport_session_id"] = $data['id'];
            $tempArray["name"] = $data['name'];
            $tempArray["destination_id"] = $data["destination_id"]; 
            $tempArray["departure_dates"] = $departureDates->loadUSSDDates($data['id']);
            
            $tempArray["busses"]=$tansportSessionBuses->fetchAllActivebuses($data['id']);
            
            array_push($resultArray, $tempArray);
        }

        return $resultArray;
    }

    
    /**
     * Used to pull information from from loaded row
     * 
     * @param string $columnName The name of the column which information is to be pulled
     * 
     * @return string The string containing the information pulled from the row
     */
    public function pullColumnInfo($columnName)
    {
        return $this->_transportSessionModel[$columnName];
    }
}
?>