<?php

namespace Cheetah\Models;


class UserReceiptModel extends Model
{
    private $_userReceipt;

    function __construct()
    {
        parent::setTable('user_receipt');
        parent::setColumns(
            [
                "user_id" => "integer",
                "receipt_id" => "integer"
            ]
        );    
    }


    /**
     * Used to pull information from from loaded row
     * 
     * @param string $columnName The name of the column which information is to be pulled
     * 
     * @return string The string containing the information pulled from the row
     */
    public function pullColumnInfo($columnName)
    {
        return $this->_transportBusSessionModel[$columnName];
    }
 
}
?>