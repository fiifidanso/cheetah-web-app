<?php 

namespace Cheetah\System;

class Alert
{
    public static function createErrorMessage($page, $contents)
    {
        $_SESSION["cheetah"]["error"]=[
            $page => $contents
        ];
        
    }

    public static function createSuccessMessage($page, $contents)
    {
        $_SESSION["cheetah"]["success"]=[
            $page => $contents
        ];
        
    }

    public static function createFormErrorMessage($page, $key, $contents, $formInput)
    {

        
        $_SESSION["cheetah"]["formError"]["error"]=[
            $page => $contents
        ];
        $_SESSION["cheetah"]["formError"]["data"]=$formInput;

        $_SESSION["cheetah"]["formError"]["key"]=$key;
    }

    public static function displayAlert($page)
    {
        if (isset($_SESSION["cheetah"]["error"])) {
            
            if (array_key_exists($page, $_SESSION["cheetah"]["error"])) {
            ?>
            <script>
            
                $(document).ready(toastr.error(
                    '<?php echo $_SESSION["cheetah"]["error"][$page];?>',
                        'Error!')
                    )
            
            </script>
            <?php
            unset($_SESSION["cheetah"]["error"][$page]);
            }
        }
        
        if (isset($_SESSION["cheetah"]["success"])) {
            
            if (array_key_exists($page, $_SESSION["cheetah"]["success"])) {
            ?>
            <script>
            
                $(document).ready(toastr.success(
                    '<?php echo $_SESSION["cheetah"]["success"][$page];?>',
                        'Success!')
                    )
            
            </script>
            <?php
            unset($_SESSION["cheetah"]["success"][$page]);
            }
        }
          
        
        if (isset($_SESSION["cheetah"]["formError"]["error"])) {
        
            if (array_key_exists($page, $_SESSION["cheetah"]["formError"]["error"])) {
            ?>
            <script>
            
            $(document).ready(toastr.error(
                '<?php echo $_SESSION["cheetah"]["formError"]["error"][$page];?>',
                'Error!')
            )
            
            $("#<?php echo $_SESSION["cheetah"]["formError"]["key"];?>").addClass("is-invalid")
            
            forms = JSON.parse('<?php echo json_encode($_SESSION["cheetah"]["formError"]["data"]); ?>')
            $.each(forms, function(key, content){
                $("#"+key).val(content)
            })
            console.log(forms)
            </script>
        <?php
            unset($_SESSION["cheetah"]["formError"]);
            }
        }
    }
}
?>