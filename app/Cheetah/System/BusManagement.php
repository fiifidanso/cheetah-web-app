<?php

namespace Cheetah\System;


class BusManagement
{
    private $_bus, $_transportBusSession, $_loadingBus;

    public function loadByBusTransportSession($id)
    {
        $this->_transportBusSession = new \Cheetah\Models\TransportBusSessionModel;
        $this->_loadingBus = new \Cheetah\Models\LoadingBusModel;

        $this->_transportBusSession->loadBusSessionById($id);
    }

    public function loadCurrentLoadingBus($date_id)
    {
        $result = $this->_loadingBus->getCurrentBusLoading(
            $this->_transportBusSession->pullColumnInfo("id"),
            $date_id
        );

        if ($result) {
            return;
        }

        $this->createNewLoadingBus($date_id);

        return $this->loadCurrentLoadingBus($date_id);
    }


    public function createNewLoadingBus($date_id)
    {

        $getBus = new \Cheetah\Models\BusTypeModel;

        $getBus->getBusTypeModelById($this->_transportBusSession->pullColumnInfo('bus_id'));

        $this->_loadingBus->save(
            [
                "bus_session_id" => $this->_transportBusSession->pullColumnInfo('id'),
                "transport_session" => $this->_transportBusSession->pullColumnInfo('transport_session_id'),
                "status" => "loading",
                "bus_type" => $this->_transportBusSession->pullColumnInfo('bus_id'),
                "empty_seats" => $getBus->pullColumnInfo("total_seats"),
                "total_seats" => $getBus->pullColumnInfo("total_seats"),
                "departure_date_id" => $date_id
            ]
        );

    }


    public function assignTicketsToBus($invoice, $receipt)
    {
        //Check if there is enough space
        if ($invoice->pullColumnInfo("number_tickets") <= $this->_loadingBus->pullColumnInfo("empty_seats")) {


        } else {
            $this->getNextBus($invoice->pullColumnInfo("number_tickets"));
        }

        $this->_loadingBus->reserveSpace($invoice->pullColumnInfo("number_tickets"));


        $tickets = new \Cheetah\Models\TicketModel;
        for ($i = 0; $i < $invoice->pullColumnInfo("number_tickets"); $i++) {
            $tickets->save(
                [
                    "loading_bus_id" => $this->_loadingBus->pullColumnInfo("id"),
                    "receipt_id" => $receipt->pullColumnInfo("id"),
                    "seat_no" => 0,
                    "status" => "active",
                    "pickup_point_id" => $invoice->pullColumnInfo("pickup_point_id"),
                    "client_id" => $receipt->pullColumnInfo("client_id"),

                ]
            );
        }

    }

    public function getNextBus($numberOfSeats)
    {
        $searchingForBus = true;
        
        //This variable starts from 2 because the first bus in wait is full and can't accomadate the space
        $busInLoadingRow = 2;


        while ($searchingForBus) {
            echo "SEarch for $busInLoadingRow now <br><br><br>";
            if ($this->_loadingBus->checkAvailableLoadingBuses($busInLoadingRow)) {
                //Check if next bus in row has enough space
                if ($numberOfSeats <= $this->_loadingBus->pullColumnInfo("empty_seats")) {
                    $searchingForBus = false;
                }
                $busInLoadingRow++;
            } else {
                //Create a new loading bus as there are none available to be used
                $searchingForBus = false;
                $this->createNewLoadingBus($this->_loadingBus->pullColumnInfo("departure_date_id"));

                $this->_loadingBus->switchToMostRecent();
            }

        }


    }
}

?>