<?php

namespace Cheetah\System;


class MobileMoney
{
    private $_currentMomo, $_decodedJSON;

    public function processResponse($input)
    {
        $this->_currentMomo = new \Cheetah\Models\Momo;

        $this->_decodedJSON = \json_decode($input);

        if (isset($this->_decodedJSON->Order_id)) {

            $this->_currentMomo->pullOutMomo($this->_decodedJSON->Order_id);

            if ($this->_currentMomo->isPresent()) {

                $this->_currentMomo->loadMomo();
                $this->resolveMomo();
            }

        }

        return false;

    }

    private function resolveMomo()
    {
        $this->_currentMomo->edit(
            [
                "id" => $this->_currentMomo->pullColumnInfo("id"),
                "status" => $this->_decodedJSON->Status,
                "invoice_no" => $this->_decodedJSON->InvoiceNo
            ]
        );

        if ($this->_decodedJSON->Status == "PAID") {
            $this->successPayment();
            return true;
        }

        echo "fail";
        return false;
    }


    private function successPayment()
    {

        $payment = new \Cheetah\System\Payment($this->_decodedJSON->Order_id);

        $details = $payment->validatePayment();

        $bus = new \Cheetah\System\BusManagement;

        $bus->loadByBusTransportSession(
            $details["invoice"]->pullColumnInfo("transport_bus_id")
        );

        $bus->loadCurrentLoadingBus(
            $details["invoice"]->pullColumnInfo("departure_date_id")
        );

        $bus->assignTicketsToBus($details["invoice"], $details["receipt"]);

        $admin = new \Cheetah\Models\Admin;

    
        $senderID = $admin->fetch_sender_id($payment->getInvoiceCompany());

        $sendSms = new \Cheetah\System\SendSms;
        $sendSms->setSenderID($senderID);

        $message = $payment->createPaymentSMS();

        $sendSms->sendSms(
            $message,
            $details["invoice"]->pullColumnInfo("msisdn"),
            $payment->getInvoiceCompany()
        );

    }
}

?>