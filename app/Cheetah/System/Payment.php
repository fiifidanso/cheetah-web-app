<?php

namespace Cheetah\System;


class Payment
{
    private $_invoice, $_receipt, $_client;

    function __construct($orderID)
    {
        $this->_invoice = new \Cheetah\Models\InvoiceModel;
        $this->_invoice->loadInvoice($orderID);
    }

    public function validatePayment()
    {
        $this->_client = new \Cheetah\Models\ClientModel;
        
        $this->_client->createClient($this->_invoice->pullColumnInfo('msisdn'));
        
        $this->_invoice->edit(
            [ 
                "status" => "paid",
                "id" => $this->_invoice->pullColumnInfo("id")
            ]
        );

        $this->_receipt =  new \Cheetah\Models\ReceiptModel;

        $receiptNo = uniqid();
        
        $this->_receipt->save(
            [
                "client_id" =>  $this->_client->pullColumnInfo("id"),
                "transport_route_id" => $this->_invoice->pullColumnInfo("transport_route_id"),
                "ticket_quantity" => $this->_invoice->pullColumnInfo("number_tickets"),
                "amount" => $this->_invoice->pullColumnInfo("amount"),
                "status" => "paid",
                "receipt_no" => $receiptNo,
                "pay_method" => $this->_invoice->pullColumnInfo("pay_method"),
                "invoice_id" =>  $this->_invoice->pullColumnInfo("id")
            ]
        );

        $this->_receipt->loadReceiptByReceiptId($receiptNo);
        
        if ($this->_invoice->pullColumnInfo("pay_method")== "cash") {
            $userReceipt = new \Cheetah\Models\UserReceiptModel;

            $extraInfo = json_decode($this->_invoice->pullColumnInfo("extra_info"));
            $userReceipt->save(
                [
                    "user_id" => $extraInfo->agentId,
                    "receipt_id" => $this->_receipt->pullColumnInfo("id")
                ]
            );
        }

        return [ 
            "receipt" => $this->_receipt,
            "invoice" => $this->_invoice
        ];
    }

    public function getInvoiceCompany()
    {
        return $this->_invoice->pullColumnInfo("company");
        
    }
    
    public function createPaymentSMS()
    {
        $transportSession = new \Cheetah\Models\TransportSessionModel;
        $transportSession->loadRouteById($this->_invoice->pullColumnInfo("transport_route_id"));

        $departureDate = new \Cheetah\Models\DepartureDatesModel;
        $departureDate->loadById($this->_invoice->pullColumnInfo("departure_date_id"));

        $departureDateDate = new \DateTime($departureDate->pullColumnInfo("date"));
        
        $departureTime = date("g:i a", strtotime($departureDate->pullColumnInfo("time")));

        $ticket = "ticket";
        if ($this->_invoice->pullColumnInfo("number_tickets") > 1 ) {
            $ticket .= "s";
        }

        $amount = number_format($this->_invoice->pullColumnInfo("amount")*0.01, 2);

        $message = "Hi, ".
            $this->_invoice->pullColumnInfo("number_tickets").
            " ".$ticket." successfully purchased for GHS ".
            $amount.". ".
            $transportSession->pullColumnInfo("name")." for ".
            $departureDateDate->format('d M Y')."@ ".
            $departureTime.". Click here to reserve seat http://bit.ly/chetaTS. Login pin:".$this->_client->pullColumnInfo('pin');
        return $message;
    
    }
}

?>