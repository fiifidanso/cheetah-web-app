<?php

namespace Cheetah\System;


class Security
{

    public function login($email, $password)
    {
        $security = new \Cheetah\Models\Admin;

        $result = $security->fetch_user($email);
        
        if ($result->num_rows) {
            $data = $result->fetch_assoc();
            $password =md5($password);
            if ($password == $data["password"]) {
                $_SESSION["cheetah"]["sessionDuration"] = time();
                $_SESSION["cheetah"]["loggedIn"] = true;
                $_SESSION["cheetah"]["name"] = $data["name"];
                $_SESSION["cheetah"]["id"] = $data["id"];
                $_SESSION["cheetah"]["level"] = $data["level"];
                $_SESSION["cheetah"]["company"] = $data["company"];
                $_SESSION["cheetah"]["addService"] = $data["is_haulage"];
                $_SESSION["cheetah"]["image"] = $data["company_img"];
                header("Location: index.php");
                exit();
            }
        }

        return false;

        //Redirect back to homepage
        //WITH ereor
    }

    public static function logout()
    {
        unset($_SESSION);
        header("Location: login.php");

    } 

    public static function checkLogin()
    {
        if (isset($_SESSION["cheetah"]["loggedIn"]) and $_SESSION["cheetah"]["loggedIn"]) {

            $remainderTime = time() - $_SESSION["cheetah"]["sessionDuration"];
            if ($remainderTime < 1500) {
                $_SESSION["cheetah"]["sessionDuration"] = time();
                return;
            } 
        }

        Security::logout();
    }

    public static function updateUSSDDatabase()
    {
        shell_exec("curl '".UPDATE_USSD_ENDPOINT."'");
        
    }


    public function loginClient($msisdn, $pin)
    {
        $client = new \Cheetah\Models\ClientModel;

        if ($client->loadClientByMsisdn($msisdn)) {
           
            if ($pin == $client->pullColumnInfo("pin")) {
                $_SESSION["cheetah"]["sessionDurationClient"] = time();
                $_SESSION["cheetah"]["clientLogin"] = true;
                $_SESSION["cheetah"]["clientName"] = $client->pullColumnInfo('name');
                $_SESSION["cheetah"]["clientId"] =  $client->pullColumnInfo('id');
                $_SESSION["cheetah"]["msisdn"] =  $client->pullColumnInfo('msisdn');
                header("Location: ticketDisplay.php");
                exit();
            }
        }

        return false;

        //Redirect back to homepage
        //WITH ereor
    }

    
    public static function checkClientLogin()
    {
        if (isset($_SESSION["cheetah"]["clientLogin"]) and $_SESSION["cheetah"]["clientLogin"]) {

            $remainderTime = time() - $_SESSION["cheetah"]["sessionDurationClient"];
            if ($remainderTime < 3000) {
                $_SESSION["cheetah"]["sessionDurationClient"] = time();
                return;
            } 
        }

        Security::logoutClient();
    }


    public static function logoutClient()
    {
        unset($_SESSION);
        header("Location: ticket.php");

    } 

    public static function checkLevel()
    {
        $agentPermissions = [
            "activeBuses" => true,
            "clientInfo" => true
        ];
        
        if ($_SESSION["cheetah"]["level"] != 'admin' and !array_key_exists($GLOBALS['currentPage'], $agentPermissions) ) {
            if ($GLOBALS['currentPage'] != "dashboard") {
               
                header("Location: index.php");
            }
          
        }
    }

    public static function getAllCompaniesAsArray()
    {
        $security = new \Cheetah\Models\Admin;

        $result = $security->getAllCompanies();
        $companies=[];

        while ($data = $result->fetch_assoc()) {
            \array_push(
                $companies,
                $data['company']
            );
        }

        return $companies;
    }
    
}

?>