<?php

namespace Cheetah\System;


class SendSms
{

    private $_cheetah_sms_username;
    private $_cheetah_sms_password;
    private $_cheetah_sms_sender_id;

    function __construct()
    {
        $this->_cheetah_sms_username = \urlencode("cheetahtransport");
        $this->_cheetah_sms_password = \urlencode("Cheetahts@gh");
        $this->_cheetah_sms_sender_id = \urlencode("Cheetah TS");

    }
    
    public function setSenderID($sender_id)
    {
        $this->_cheetah_sms_sender_id = $sender_id;
    }
    

    public function sendSms($message, $msisdn, $company)
    {

        $messageEnc = \urlencode($message);
        $url = SMS_END_POINT .
            "username=" . $this->_cheetah_sms_username .
            "&password=" . $this->_cheetah_sms_password . "&" .
            "type=0&dlr=1&destination=$msisdn" .
            "&source=" . $this->_cheetah_sms_sender_id . "&message=" . $messageEnc;


        $response = shell_exec("curl '$url'");
        $messageModel = new \Cheetah\Models\MessageModel;
        
        $messageModel->save(
            [
                "msisdn" => $msisdn,
                "message" => $message,
                "response" => $response,
                "customer_id" => $this->_cheetah_sms_sender_id,
                "company" => $company
            ]
        );

    }

}

?>