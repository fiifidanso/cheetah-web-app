<?php

require_once "../start.php";


if ($_POST['method'] == 'edit') {
   
    $editAgent = new \Cheetah\Forms\EditAgentForm;
    
    $editAgent->editAgent();
    \Cheetah\System\Alert::createSuccessMessage(
        "Agents",
        'Agent Edited!'
    );

    Cheetah\System\Security::updateUSSDDatabase();
    

} elseif ($_POST['method'] == 'delete') {
     $deleteAgent = new \Cheetah\Forms\DeleteGenericForm("Agents");

    $deleteAgent->deleteForm("users");

    \Cheetah\System\Alert::createSuccessMessage(
        "Agents",
        'Agent Deleted!'
    );
   
}
header("Location: ".$_SERVER["HTTP_REFERER"]);
   
?>