<?php

require_once "../start.php";

$agentForm = new Cheetah\Forms\AgentForm;

if ($agentForm->saveForm()) {
    \Cheetah\System\Alert::createSuccessMessage(
        "Agents",
        'Agent Added!'
    );


    Cheetah\System\Security::updateUSSDDatabase();
        
} else {
    \Cheetah\System\Alert::createErrorMessage(
        "Agents",
        'SYSTEM ERROR!'
    );
}

header("Location: ".$_SERVER["HTTP_REFERER"]);
?>