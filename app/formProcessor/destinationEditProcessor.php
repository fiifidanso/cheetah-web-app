<?php

require_once "../start.php";


if ($_POST['method'] == 'edit') {
   
    $editDestination = new \Cheetah\Forms\EditDestinationForm;


    if ($editDestination->editDestination()) {
        \Cheetah\System\Alert::createSuccessMessage(
            "Destination",
            'Destination Edited!'
        );


        Cheetah\System\Security::updateUSSDDatabase();
        header("Location: ".$_SERVER["HTTP_REFERER"]);
        
    } else {
        \Cheetah\System\Alert::createErrorMessage(
            "Destination",
            'SYSTEM ERROR!'
        );
    }
   

} elseif ($_POST['method'] == 'delete') {
     $deleteDestination = new \Cheetah\Forms\DeleteGenericForm("Destination");

    $deleteDestination->deleteForm("destination");

    \Cheetah\System\Alert::createSuccessMessage(
        "Destination",
        'Destination Deleted!'
    );
    header("Location: ".$_SERVER["HTTP_REFERER"]);
}

?>