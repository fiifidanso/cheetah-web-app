<?php

require_once "../start.php";

$destinationForm = new Cheetah\Forms\DestinationForm;

if ($destinationForm->saveForm()) {
    \Cheetah\System\Alert::createSuccessMessage(
        "Destination",
        'Destination Added!'
    );


    Cheetah\System\Security::updateUSSDDatabase();

    header("Location: ".$_SERVER["HTTP_REFERER"]);
        
} else {
    \Cheetah\System\Alert::createErrorMessage(
        "Destination",
        'SYSTEM ERROR!'
    );
}

?>