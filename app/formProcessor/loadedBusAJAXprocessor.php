<?php 

require_once "../start.php";


$parameters = filter_input_array(INPUT_GET, FILTER_DEFAULT);

$loadingBus = new Cheetah\Models\LoadingBusModel;



$output = [
    "data" => $loadingBus->drawTickets()
];

echo json_encode($output);
?>