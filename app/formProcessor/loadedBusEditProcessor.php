<?php

require_once "../start.php";


$parameters = filter_input_array(INPUT_POST, FILTER_DEFAULT);

$loadingBus = new Cheetah\Models\LoadingBusModel;

$loadingBus->edit(
    [
        "id" => $parameters["editID"],
        "status" => "completed"]
);

\Cheetah\System\Alert::createSuccessMessage(
    "activeBuses",
    'Transport Session Edited'
);

header("Location: ".$_SERVER["HTTP_REFERER"]);
   

?>