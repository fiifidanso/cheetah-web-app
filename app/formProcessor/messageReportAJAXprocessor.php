<?php 

require_once "../start.php";


$parameters = filter_input_array(INPUT_GET, FILTER_DEFAULT);

$invoice = new Cheetah\Models\MessageModel;



$output = [
    "data" => $invoice->drawReport()
];

echo json_encode($output);
?>