<?php 

require_once "../start.php";


$parameters = filter_input_array(INPUT_GET, FILTER_DEFAULT);


$admin = new Cheetah\Models\Admin;
$senderID = $admin->fetch_sender_id($_SESSION["cheetah"]["company"]);

$sendSms = new Cheetah\System\SendSms;

$sendSms->setSenderID($senderID);

$sendSms->sendSms(
    $parameters["message"],
    $parameters["msisdn"],
    $_SESSION["cheetah"]["company"]
);

?>