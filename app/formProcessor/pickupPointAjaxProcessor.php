<?php 

require_once "../start.php";


$parameters = filter_input_array(INPUT_POST, FILTER_DEFAULT);

$pickupPoints = new Cheetah\Models\PickupPointsModel;

switch ($parameters['method']) {
case "pullPoints":
    echo json_encode($pickupPoints->getAllPoints($parameters['id']));
    return;
    break;
case "savePoint":
    if ($pickupPoints->save(
        [
            "school_id" => $parameters["school_id"],
            "name" => $parameters["name"],
            "status" => "active"
        ]
    )
    ) {
        echo '1';
        return;
    };
    break;
case "editPoint":
    if ($pickupPoints->edit(
        [
            "id" => $parameters["id"],
            "name" => $parameters["name"]
        ]
    )
    ) {
        echo '1';
        return;
    };
    break;
case "deletePoint";
    if (\Cheetah\Models\Model::deleteEntry(
        $parameters['id'],
        'pickup_points'
    )
    ) {
        echo '1';
        return;
    }
    ;

    break;
}

echo '2';
?>