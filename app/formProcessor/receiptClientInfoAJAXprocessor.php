<?php 

require_once "../start.php";


$parameters = filter_input_array(INPUT_GET, FILTER_DEFAULT);

$client = new Cheetah\Models\ClientModel;



$output = [
    "data" => $client->drawClientInfoTable()
];

echo json_encode($output);
?>