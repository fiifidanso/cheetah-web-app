<?php 

require_once "../start.php";


$parameters = filter_input_array(INPUT_GET, FILTER_DEFAULT);

$receipt = new Cheetah\Models\ReceiptModel;



$output = [
    "data" => $receipt->drawReport()
];

echo json_encode($output);
?>