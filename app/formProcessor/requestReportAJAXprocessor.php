<?php 

require_once "../start.php";


$parameters = filter_input_array(INPUT_GET, FILTER_DEFAULT);

$request = new Cheetah\Models\RequestModel;



$output = [
    "data" => $request->drawReport()
];

echo json_encode($output);
?>