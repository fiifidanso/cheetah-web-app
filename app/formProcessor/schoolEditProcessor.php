<?php

require_once "../start.php";


if ($_POST['method'] == 'edit') {
   
    $editSchool = new \Cheetah\Forms\EditSchoolForm;

    $editSchool->editSchool();
    //var_dump($_POST);
    
    $editSchool->setDestination();
    \Cheetah\System\Alert::createSuccessMessage(
        "Schools",
        'Destination(s) Added!'
    );
    header("Location: ".$_SERVER["HTTP_REFERER"]);
   

} elseif ($_POST['method'] == 'delete') {
    $deleteSchool = new \Cheetah\Forms\DeleteGenericForm("Schools");

    $deleteSchool->deleteForm("schools");

    \Cheetah\System\Alert::createSuccessMessage(
        "Schools",
        'School Deleted!'
    );
    header("Location: ".$_SERVER["HTTP_REFERER"]);
}

?>