<?php

require_once "../start.php";

$schoolForm = new Cheetah\Forms\SchoolForm;

if ($schoolForm->saveForm()) {
    \Cheetah\System\Alert::createSuccessMessage(
        "Schools",
        'School Added!'
    );
  
    
    Cheetah\System\Security::updateUSSDDatabase();

} else {
    \Cheetah\System\Alert::createErrorMessage(
        "Schools",
        'SYSTEM ERROR!'
    );
}
header("Location: ".$_SERVER["HTTP_REFERER"]);
?>