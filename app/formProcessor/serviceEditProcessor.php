<?php

require_once "../start.php";


if ($_POST['method'] == 'edit') {
   
    $editService = new \Cheetah\Forms\EditServiceForm;
    
    $editService->editService();
    \Cheetah\System\Alert::createSuccessMessage(
        "Service",
        'Service Edited!'
    );

    Cheetah\System\Security::updateUSSDDatabase();
    

} elseif ($_POST['method'] == 'delete') {
     $deleteService = new \Cheetah\Forms\DeleteGenericForm("Service");

    $deleteService->deleteForm("bus_types");

    \Cheetah\System\Alert::createSuccessMessage(
        "Service",
        'Service Deleted!'
    );
   
}
header("Location: ".$_SERVER["HTTP_REFERER"]);
   
?>