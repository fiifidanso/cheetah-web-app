<?php

require_once "../start.php";

$serviceForm = new Cheetah\Forms\ServiceForm;

if ($serviceForm->saveForm()) {
    \Cheetah\System\Alert::createSuccessMessage(
        "Service",
        'Service Added!'
    );


    Cheetah\System\Security::updateUSSDDatabase();
        
} else {
    \Cheetah\System\Alert::createErrorMessage(
        "Service",
        'SYSTEM ERROR!'
    );
}

header("Location: ".$_SERVER["HTTP_REFERER"]);
?>