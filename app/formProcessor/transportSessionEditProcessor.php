<?php

require_once "../start.php";


if ($_POST['method'] == 'edit') {
    $ediTtransportSession = new Cheetah\Forms\EditTransportSessionForm;
    
    $ediTtransportSession->editSession();
    
    \Cheetah\System\Alert::createSuccessMessage(
        "transportSession",
        'Transport Session Edited'
    );

    Cheetah\System\Security::updateUSSDDatabase();
    
    header("Location: ".$_SERVER["HTTP_REFERER"]);
   

} elseif ($_POST['method'] == 'delete') {

    $deleteSchool = new \Cheetah\Forms\DeleteGenericForm("transportSession");

    $deleteSchool->deleteForm("transport_session");

    \Cheetah\System\Alert::createSuccessMessage(
        "transportSession",
        'Transport Session Deleted!'
    );
    header("Location: ".$_SERVER["HTTP_REFERER"]);
}

?>