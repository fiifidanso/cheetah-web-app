<?php

require_once "../start.php";

$transportSession = new Cheetah\Forms\TransportSessionForm;

if ($transportSession->saveForm()) {
    \Cheetah\System\Alert::createSuccessMessage(
        "transportSession",
        'Transport Session Added!'
    );

    Cheetah\System\Security::updateUSSDDatabase();
    
    header("Location: ../../transportSession.php");
} else {
    \Cheetah\System\Alert::createErrorMessage(
        "newSession",
        'SYSTEM ERROR!'
    );
    header("Location: ".$_SERVER["HTTP_REFERER"]);
}

?>