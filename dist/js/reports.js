function checkDates() {
    startDate = $("#startDate").val()
    endDate = $("#endDate").val()


    if (startDate > endDate && endDate != "") {
        $(document).ready(toastr.error(
            'Start date greater than end Date',
            'Error!'))

        return false
    }

    if (startDate == "") {
        toastr.error(
            'Select Date',
            'Error!')

        return false
    }
    if (startDate == "" && endDate != "") {
        toastr.error(
            'Input a date for start date',
            'Error!')


        return false
    }

    return true

}


function drawTitle(name, preHeading, postHeading) {
    startDate = $("#startDate").val()
    endDate = $("#endDate").val()


    title = "Report for " + preHeading + " " + name;

    if (endDate == "") {
        title += " for " + startDate
    } else {
        title += " from " + startDate + " to " + endDate
    }

    $("#reportHeading").html(title)
}

function addCommas(nStr, moneyOption) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    if (moneyOption) {
        x2 = x.length > 1 ? '.' + x[1].substring(0, 2) : '.00';
        x2 = x2.length < 3 ? x2 + '0' : x2;
    } else {
        x2 = x.length > 1 ? '.' + x[1] : '';
    }
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function checkIfPhoneNumber(phoneNumber)
{
    if(isNaN(phoneNumber)) {
        
        $(document).ready(toastr.error(
            'Enter a valid Mobile Number',
            'Error!'))
        return false

    } else {
        
        if (phoneNumber.length == 10 && phoneNumber.charAt(0) == 0 ) {
            return true
        } else if (phoneNumber.length == 12 && phoneNumber.substring(0,3) == '233' ){
            return true
        }
        $(document).ready(toastr.error(
            'Enter a valid Mobile Number',
            'Error!'))
        return false

    }
}