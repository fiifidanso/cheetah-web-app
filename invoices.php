<?php 

$currentPage = "invoices";
require_once "layout/header.php";
require_once "layout/sidebar.php";


?>
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Invoices</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Invoices</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
           
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Filter</h5>
                        <form class="form-horizontal" action="app/formProcessor/schoolProcessor.php" method="post">
                            <input autocomplete="false" name="hidden" type="text" style="display:none;">
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="fname" class="text-right control-label col-form-label">Start Date:</label>
                                    <div class="">
                                        <input autocomplete="false" placeholder="mm/dd/yyyy" type="date" class="form-control" id="startDate" name="schoolName" >
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="fname" class="text-right control-label col-form-label">End Date:</label>
                                    <div class="">
                                        <input autocomplete="false" placeholder="mm/dd/yyyy" type="date" class="form-control" id="endDate" name="schoolLocation">
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="fname" class="text-right control-label col-form-label">Status:</label>
                                    <div class="">
                                        <select class="form-control" id="invoiceStatus" >
                                            <option value="all">All</option>
                                            <option value="pending">Pending</option>
                                            <option value="paid">Paid</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="fname" class="text-right control-label col-form-label"></label>
                                    <button type="button" class="btn btn-primary" id="searchResult" style="padding-top: 0px;margin-top: 35px;">Search</button>
                                </div>
                            </div>
                        </form>
                       
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title" id="reportHeading">Invoices</h5>
                        <table id="invoicesTable" class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Number</th>
                                    <th scope="col">Transport Session</th>
                                    <th scope="col">Departure Date</th>
                                    <th scope="col">Number Tickets</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Timestamp</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php 
require_once "layout/footer.php";
?>