
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="dist/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <script src="assets/libs/toastr/build/toastr.min.js"></script>
    <script src="dist/js/formManager.js"></script>

   <?php 
    Cheetah\System\Alert::displayAlert($currentPage);

    switch ($currentPage) {

    case "Schools":
        ?>
            <script>

            function fillEditModal(id, name, location, destination) {
                    $("#editSchoolName").val(name)
                    $("#editSchoolLocation").val(location)
                    $("#editId").val(id)
                    $('input:checkbox').prop('checked',false)
                    $('#currentschoolid').val(id)
            
                    destinationArray =destination.split('||')
                
                    if (destinationArray[0] != "" ) {
                        $.each(destinationArray , function(index,value){
                            $("#"+value).prop('checked', true)
                        })
                    }
                    loadPickupPoints(id)
                }

            

                function fillDeleteModal(id, text) {
                    $("#schoolDeleteName").text(text)
                    $("#deleteId").val(id)
                }

                function addNewPickupRow() {
                    $("#pickuppointbody").append(
                    "<tr><td><input type='text'/></td> "+
                    "<td><a  href='#' style='color: #00A269 ' onclick='savePickupRow($(this))'> "+
                    "<i class='m-r-10 mdi mdi-check ' data-toggle='tooltip'  "+
                    "data-placement='top' title='' data-original-title='Save'></i> "+
                    "</a><a onclick='' href='#' style='color: #0088cc'> "+
                    "<i class='m-r-10 mdi mdi-plus danger' data-toggle='tooltip' data-placement='top' title='' "+ 
                    "onclick='addNewPickupRow()' "+
                    "data-original-title='New Row'></i>"+
                    "</a></td></tr>")
                }

                function loadPickupPoints(id)
                {
                    loadLoader('pickuppointid')
                    $.ajax({
                        url : 'app/formProcessor/pickupPointAjaxProcessor.php',
                        type : 'post',
                        data : {
                            id : id,
                            method : 'pullPoints'
                        },
                        success : function (data) {
                            console.log(data)
                            drawPickUpPointTable(data)
                        }
                    }) 
                }

                function drawPickUpPointTable(result)
                {
                    $('#pickuppointid').html('<table class="table"><thead><tr><th scope="col">Pickup Point</th>'+
                    '<th scope="col">Action</th></tr></thead>'+
                    '<tbody id="pickuppointbody" ></tbody></table>')
                    console.log(result)
                    resultArray = JSON.parse(result)

                    $.each(resultArray, function(index, content){
                        $("#pickuppointbody").append(
                    "<tr><td><input type='text' value='"+content.name+"'/></td> "+
                    "<td><a  href='#' style='color: #00A269 ' onclick='editPickupRow("+content.id+", $(this))'> "+
                    "<i class='m-r-10 mdi mdi-check ' data-toggle='tooltip'  "+
                    "data-placement='top' title='' data-original-title='Edit'></i> "+
                    "</a><a onclick='deletePickupRow("+content.id+")' href='#' style='color: #ff294d'> "+
                    "<i class='m-r-10 mdi mdi-close danger' data-toggle='tooltip' "+
                    "data-placement='top' title='' data-original-title='Delete'></i> "+
                    "</a></td></tr>")
                
                    })
                    addNewPickupRow()
                }

                function savePickupRow(row) {
                    namePoint = row.closest("tr").find("input").val()
                    if (namePoint.length < 1) {

                        toastr.error('Input a valid Pick Up Point Name', 'Error!')
                        return
                    }
                    loadLoader('pickuppointid')
                    $.ajax({
                        url : 'app/formProcessor/pickupPointAjaxProcessor.php',
                        type : 'post',
                        data : {
                            school_id : $('#currentschoolid').val(),
                            name : namePoint,
                            method : 'savePoint'
                        },
                        success : function (data) {
                            console.log(data)
                            if (data == '1') {
                                toastr.success("Pickup Point Added", "Success!")
                                loadPickupPoints($('#currentschoolid').val())
                            }else {
                                toastr.error('System Error Contact Admin', 'Error!')
                            }
                        }
                    })
                }

                function editPickupRow(id, row) {
                    namePoint = row.closest("tr").find("input").val()
                    if (namePoint.length < 1) {

                        toastr.error('Input a valid Pick Up Point Name', 'Error!')
                        return
                    }
                    loadLoader('pickuppointid')
                    $.ajax({
                        url : 'app/formProcessor/pickupPointAjaxProcessor.php',
                        type : 'post',
                        data : {
                            id : id,
                            name : namePoint,
                            method : 'editPoint'
                        },
                        success : function (data) {
                            console.log(data)
                            if (data == '1') {
                                toastr.success("Pickup Point Edited", "Success!")
                                loadPickupPoints($('#currentschoolid').val())
                            }else {
                                toastr.error('System Error Contact Admin', 'Error!')
                            }
                        }
                    })
                }

                function deletePickupRow(id) {
                    loadLoader('pickuppointid')
                    $.ajax({
                        url : 'app/formProcessor/pickupPointAjaxProcessor.php',
                        type : 'post',
                        data : {
                            id : id,
                            method : 'deletePoint'
                        },
                        success : function (data) {
                            console.log(data)
                            if (data == '1') {
                                toastr.success("Pickup Point Deleted", "Success!")
                                loadPickupPoints($('#currentschoolid').val())
                            }else {
                                toastr.error('System Error Contact Admin', 'Error!')
                            }
                        }
                    })
                }
            </script>
        <?php
        break;


    case "Destination":
                ?>
            <script>
                function fillEditModal(id, text) {
                    $("#editDestinationName").val(text)
                    $("#editId").val(id)
                }

                function fillDeleteModal(id, text) {
                    $("#destinationDeleteName").text(text)
                    $("#deleteId").val(id)
                }
                
            </script>
            <?php
        break;
    case "Agents":
        ?>
        <script>
            function fillEditModal(id, agentName, agentNumber, agentCompany) {
                $("#editAgentId").val(id)
                $("#editAgentName").val(agentName)
                $("#editAgentNumber").val(agentNumber)
                $("#editCompany").val(agentCompany)
            }
            
            function fillDeleteModal(id, text) {
                $("#agentDeleteName").text(text)
                $("#deleteId").val(id)
            }
        </script>   
        <?php 
        break; 
    case "Service":
            ?>
        <script>
            function fillEditModal(id, name) {
                $("#editServiceId").val(id)
                $("#editServiceName").val(name)
              
            }
            
            function fillDeleteModal(id, text) {
                $("#agentDeleteName").text(text)
                $("#deleteId").val(id)
            }
        </script>   
        <?php 
        break;
    case "transportSession";
        ?>
        <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
        <script src="assets/extra-libs/DataTables/datatables.min.js"></script>
        <script>
            $("#transportTable").DataTable();

            var busSelected = 0

            function selectSessionRow(id, cell, buses, dates) {

                $(".bus-type-box").each(function(){
                    $(this).prop("checked", false);
                
                })

                $(".bus-amount").each(function(){
                    $(this).val("");
                
                })
                

                selectedBuses =buses.split('||')
                busSelected = selectedBuses.length

                
                if (selectedBuses[0] == "") 
                {
                    busSelected = 0

                } else {

                    $.each(selectedBuses, function(index, value){
                        busInfo = value.split('|-|')
                    
                        $("#"+busInfo[0]+"_bus_id").prop("checked", true);
                        $("#bus_amount_"+busInfo[0]).attr('required', 'true')
                        $("#bus_amount_"+busInfo[0]).val(busInfo[1])
                        $("#bus_momo_"+busInfo[0]).val(busInfo[2])
                        $("#bus_nhil_"+busInfo[0]).val(busInfo[3])

                    })
                }

                // currentdates = dates.split('||')
                
                // $("#depatureDatesTable").html('')

                // if (currentdates[0] == "")
                // {
                //     $("#depatureDatesTable").html(
                //         '<tr>'+
                //         '<td>'+
                //         '<div class="">'+
                //         '<input required type="date" class="form-control depatureDates"  class="date">'+
                //         '</div>'+
                //         '</td>'+
                //         '<td>'+
                //         '<td>'+
                //         '<a  style="color: #00A269 " onclick="addDateRow()">'+
                //         '<i class="m-r-10 mdi mdi-plus  info" data-toggle="tooltip" '+
                //         'data-placement="top" title="" data-original-title="Add"></i> '+
                //         '</a>'+
                //         '</td>'+
                //         '</td>'+
                //         '</tr>'
                //     )
                // } else {
                //     status = 1
                //     $.each(currentdates, function(index, value){
                        
                //         addDateRowEdit(value, status)
                //         status++
                //     })
                // }
                
                
                currentRow=cell.closest('tr')
                name=currentRow.find('td:nth-child(1)').text();
                status = currentRow.find('td:nth-child(4)').text().toLowerCase().trim();

                $("#editSessionName").text(name)
                $("#editId").val(id)

                if ( status == "pending")
                {
                $("#pendingRadioButton").prop("checked", true);
                }
                else if ( status == "active")
                {  
                    $("#activeRadioButton").prop("checked", true);
                }
                else if (  status == "paused")
                {   
                    $("#pausedRadioButon").prop("checked", true);
                }
                
            }



            $("#editTransportSessionForm").submit( function(form){
                
                
                error = 0
                
                startDate = $("#editStartDate").val()
                endDate = $("#editEndDate").val()
                
                if ( startDate > endDate ) {
                    error++
                    toastr.error('Start Date must be lesser than End Date')
                }
                if (busSelected < 1)
                {
                    error++
                    toastr.error('Select at least one bus type')
                    
                }

                if (error) {
                    form.preventDefault()
                }
            
                var busesSelected = Array()
        
                $(".bus-type-box").each(function(){
                
                    if ($( this ).is(':checked'))
                    {
                        currentId=$( this ).data('bus-id')
                        busesSelected[currentId] = {
                            "amount" : $("#bus_amount_"+currentId).val(),
                            "momo_charge" : $("#bus_momo_"+currentId).val(),
                            "nhil" : $("#bus_nhil_"+currentId).val()
                        }
                    
                    }
                })
                $("#selectedBuses").val(JSON.stringify(busesSelected))
                $("#allEditDates").val(JSON.stringify(getAllDates()))
                
                

            }

            )

            function fillDeleteModal(id, text) {
                $("#deleteName").text(text)
                $("#deleteId").val(id)
            }
            // <div class="form-group col-md-3">
            //                                 <label for="fname" class="text-right control-label col-form-label">End Date:</label>
            //                                 <div class="">
            //                                     <input autocomplete="false" placeholder="mm/dd/yyyy" type="date" class="form-control" id="endDate" name="schoolLocation">
            //                                 </div>
            //                             </div>
            //                             <div class="form-group col-md-3">
            //                                 <label for="fname" class="text-right control-label col-form-label">Status:</label>
            //                                 <div class="">
            //                                     <select class="form-control" id="invoiceStatus" >
            //                                         <option value="all">All</option>
            //                                         <option value="pending">Pending</option>
            //                                         <option value="paid">Paid</option>
            //                                     </select>
            //                                 </div>
            //                             </div>

            function addDateRow()
            {
                $("#depatureDatesTable").append(
                    '<tr>'+
                    '<td>'+
                    '<div class="">'+
                    '<input required type="date" class="form-control depatureDates"  class="date" placeholder="mm/dd/yyyy">'+
                    '</div>'+
                    '</td>'+
                    '<td>'+
                    '<td>'+
                    '<a  style="color: #00A269 " onclick="addDateRow()">'+
                    '<i class="m-r-10 mdi mdi-plus " data-toggle="tooltip" data-placement="top" title="" data-original-title="Add"></i>'+ 
                    '</a>'+
                    '<a onclick="deleteDateRow($(this))"  style="color: #ff294d"> '+
                    '<i class="m-r-10 mdi mdi-close danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>'+
                    '</a>'+
                    '</td>'+
                    '</td>'+
                    '</tr>'
                )
            }

            function addDateRowEdit(date, status)
            {
                string = '<tr>'+
                    '<td>'+
                    '<div class="">'+
                    '<input required type="date" class="form-control depatureDates"  class="date" value="'+date+'" placeholder="mm/dd/yyyy">'+
                    '</div>'+
                    '</td>'+
                    '<td>'+
                    '<td>'+
                    '<a  style="color: #00A269 " onclick="addDateRow()">'+
                    '<i class="m-r-10 mdi mdi-plus " data-toggle="tooltip" data-placement="top" title="" data-original-title="Add"></i>'+ 
                    '</a>'

                if ( status > 1) {

                    console.log('here')
                    string +='<a onclick="deleteDateRow($(this))"  style="color: #ff294d"> '+
                    '<i class="m-r-10 mdi mdi-close danger" data-toggle="tooltip" data-placement="top" title=""'+
                    ' data-original-title="Delete"></i>'+
                    '</a>'
                }

                string += '</td>'+
                    '</td>'+
                    '</tr>'

                $("#depatureDatesTable").append(
                    string 
                )

            }

            function deleteDateRow(cell)
            {
                namePoint = cell.closest("tr").html("")
            
            }

            function getAllDates()
            {
                content = [];
                $(".depatureDates").each(function(){
                    date = $(this).val()
                    
                    if ( $.inArray(date, content) == -1 ) {
                        content.push(date)
                    }
                    
                })

                return content;
            }
            
        </script>
        <?php
    case "newSession":
        ?>
    
        <link rel="stylesheet" type="text/css" href="assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <script src="assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
            
        <script>
            
            
            var busSelected = 0

            $("#schoolSelect").change(function()
                {
                $("#destinationSelect").attr('disabled', 'true')
                $.ajax({
                    url : 'app/formProcessor/newSessionAjaxProcessor.php',
                    type : 'get',
                    data : {
                        schoolId :$(this).val(),
                        method : 'getSchoolDestination'
                    },
                    success : function (data) {
                    
                        activeDestinations = JSON.parse(data)
                        resetDestinationSelect()
                        $.each(activeDestinations , function(index,value){
                            $("#destinationSelect").append('<option value="'+value.id+'">'+value.name+'</option>')
                        })
                        createSessionName()
                
                    }
                })
                }
            )

            $("#destinationSelect").change(function() {
                createSessionName()
            })

            function createSessionName()
            {
                $('#sessionName').val( $('#schoolSelect :selected').text()+' to '+$('#destinationSelect :selected').text())
            
            }

            function resetDestinationSelect() {
                $("#destinationSelect").removeAttr('disabled')
                $("#destinationSelect").html('')
        
            }
            
            $("#transportSessionForm").submit( function(form){
                error = 0
                
                school = $("#schoolSelect").val()
                if (school == null) {
                    error++
                    toastr.error('Select a school')
                }
                
                if (busSelected < 1)
                {
                    error++
                    toastr.error('Select at least one bus type')
                    
                }

                if (error) {
                    form.preventDefault()
                }
            
                var busesSelected = Array()
        
                $(".bus-type-box").each(function(){
                
                    if ($( this ).is(':checked'))
                    {
                        currentId=$( this ).data('bus-id')
                        busesSelected[currentId] = {
                            "amount" : $("#bus_amount_"+currentId).val(),
                            "momo_charge" : $("#bus_momo_"+currentId).val(),
                            "nhil" : $("#bus_nhil_"+currentId).val()
                        }
                    }
                })
                
                $("#selectedBuses").val(JSON.stringify(busesSelected))
                $("#allDates").val(JSON.stringify(getAllDatesWithTime()))
                
                
            }

            )

            $(".bus-type-box").click(function(){
                if ($(this).is(':checked'))
                {
                    currentId = $(this).data('bus-id')
                    $("#bus_amount_"+currentId).attr('required', 'true')
                    busSelected ++
                
                } else {
                    currentId = $(this).data('bus-id')
                    $("#bus_amount_"+currentId).removeAttr('required')
                    busSelected --
                }
                }
            )

            function addDateRow()
            {

                console.log("ss")
                $("#depatureDatesTable").append(
                    '<tr>'+
                    '<td>'+
                    '<div class="row">'+
                    '<div class="col-md-10">'+
                    '<input required type="date" class="form-control depatureDates"  class="date" placeholder="mm/dd/yyyy">'+
                    '</div>'+
                    '<div class=col-md-2>'+
                    '<a  style="color: #00A269 " onclick="addDateRow()">'+
                    '<i class="m-r-10 mdi mdi-plus " data-toggle="tooltip" data-placement="top" title="" data-original-title="Add"></i>'+ 
                    '</a>'+
                    '<a onclick="deleteDateRow($(this))"  style="color: #ff294d"> '+
                    '<i class="m-r-10 mdi mdi-close danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>'+
                    '</a>'+
                    '</div>'+
                    '</div>'+
                    '</td>'+
                    '<td>'+
                    '<div class="row" >'+
                    '<div class="col-md-10"> '+
                    '<input required type="time" class="form-control depatureTime"  class="date">'+
                    '</div>'+
                    '<div class="col-md-2">'+
                    '<a  style="color: #00A269 " onclick="addTimeField($(this))">'+
                   ' <i class="m-r-10 mdi mdi-plus  info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add"></i>'+
                   ' </a>'+
                    '</div>'+
                    '</div>'+
                    '</td>'+
                    '</tr>'
                )
            }

            function addTimeField(cell) 
            {
                cell.closest("td").append('<div class="row">'+
                    '<div class="col-md-10"> '+
                    '<input required type="time" class="form-control depatureTime"  class="date">'+
                    '</div>'+
                    '<div class="col-md-2" onclick="deleteThisTime($(this))">'+
                    '<a  style="color: #ff294d"> <i class="m-r-10 mdi mdi-close danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i></a>'+
                   '</div>'+
                   '</div>'
                )
            }

            function deleteDateRow(cell)
            {
                namePoint = cell.closest("tr").html("")
            
            }

            function deleteThisTime(cell){
                namePoint = cell.parent("div").html("")
            
            }

            function getAllDatesWithTime()
            {
                content = {};
                $(".depatureDates").each(function(){
                    date = $(this).val()
                    
                    if (content[date] == undefined ) {
                        content[date] = getTime($(this))
                    }
                    
                })

                return content;
            }

            function getTime(dateInput) {
                departureTimes = []

                dateInput.closest("td").next("td").find("input.depatureTime").each(function(){
                    time = $(this).val()
                    if ( $.inArray(time, departureTimes) == -1 ) {
                        departureTimes.push(time)
                    }
                   
                })

                return departureTimes;
            }
        </script>
        <?php
        break;
    case "activeBuses":
            ?>
            
        <link rel="stylesheet" type="text/css" href="assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <script src="assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
        <script src="assets/extra-libs/DataTables/datatables.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/b-1.5.4/b-html5-1.5.4/b-print-1.5.4/datatables.min.js"></script>
        <script src="dist/js/reports.js"></script>
            
        <script>

            jQuery('.dates').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            reportTable = $("#reportTable").DataTable()
            loadedBusTable = $("#loadedBusTable").DataTable({
                dom: 'Bfrtip',
                buttons: [

                    {extend:'csv',
                    title: function(){ return titleLoadedBus },
                    },
                    {extend:'excel',
                        title: function(){ return titleLoadedBus },
                    },
                    {extend:'pdf',
                    title: function(){ return titleLoadedBus },
                    },
                    {extend:'print',
                    title: function(){ return titleLoadedBus },
                }
                ]
            })

            titleLoadedBus = "This is loaded"

            function loadTable(id, cell)
            {
                row = cell.closest("tr")
                busRoute = row.find('td:nth-child(1)').text();
                busType = row.find('td:nth-child(2)').text();
                departureDate = row.find('td:nth-child(3)').text();
                
                
                titleLoadedBus = busRoute+" on "+busType+". Departure Date:"+departureDate
                
                $("#loadedBusTitle").html(titleLoadedBus)
                $("#editID").val(id)
                
                url = "app/formProcessor/loadedBusAJAXprocessor.php?busID="+id
                loadedBusTable.ajax.url(url).load();
            }

            
            $("#searchResult").click(function(){
                
                drawActiveBusesTitle()
                
                depDate =  $("#depDate").val()
                school =  $("#schools").val()
                status = $("#busStatus").val()
                url = "app/formProcessor/loadingBusReportAJAXprocessor.php?depDate="+depDate+"&school="+school+"&status="+status
            
                reportTable.ajax.url(url).load();

                
            })

            function drawActiveBusesTitle()
            {
                title = "List of active "
                
                depDate =  $("#depDate").val()
                school =  $("#schools").val()
                status = $("#busStatus").val()
                


                if (status != "all") {
                    title += status
                }

                title += " buses "
                if (depDate != "") {
                    title += " departing on " + depDate
                } 
                if (school != "all") {
                    title += " from " +  $("#schools :selected").text()
                }


                $("#reportHeading").html(title)
            }


        </script>
        <?php
        break;
    case "invoices":
        ?>
            
        <link rel="stylesheet" type="text/css" href="assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <script src="assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
        <script src="assets/extra-libs/DataTables/datatables.min.js"></script>
        <script src="dist/js/reports.js"></script>
            
        <script>
        
            jQuery('.dates').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
        
            reportTable = $("#invoicesTable").DataTable()
        
            $("#searchResult").click(function(){
                
                drawTitle("Invoices", $("#invoiceStatus").val(), "")
                if (checkDates()) {
                    startDate =  $("#startDate").val()
                    endDate =  $("#endDate").val()
                    status = $("#invoiceStatus").val()
                    url = "app/formProcessor/invoiceReportAJAXprocessor.php?startDate="+startDate+"&endDate="+endDate+"&status="+status
                    console.log(url)
                    reportTable.ajax.url(url).load();
                }
                
            })
        </script>
        <?php
        break;
    case "Receipts":
        ?>
                   
        <link rel="stylesheet" type="text/css" href="assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <script src="assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
        <script src="assets/extra-libs/DataTables/datatables.min.js"></script>
        <script src="dist/js/reports.js"></script>
                        
        <script>
            
                
            reportTable = $("#reportTable").DataTable({
                "footerCallback": function ( row, data, start, end, display ) {
                    
                    var api = this.api(), data;
                
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                        i : 0;
                    };
                    varColumnsMoney=[3]
                    varColumns=[2]

                    
                    $.each(varColumns,function(index,value){
                                        total = api
                                .column( value )
                                .data()
                                .reduce( function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0 );
                        
                                // Total over this page
                                pageTotal = api
                                .column( value, { page: 'current'} )
                                .data()
                                .reduce( function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0 );
                        
                                // Update footer
                                $( api.column( value ).footer() ).html(
                                    addCommas(total,0)	
                                //pageTotal +'of <br>('+ total +')'
                                );		 
                            })
                            $.each(varColumnsMoney,function(index,value){
                                        total = api
                                .column( value )
                                .data()
                                .reduce( function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0 );
                        
                                // Total over this page
                                pageTotal = api
                                .column( value, { page: 'current'} )
                                .data()
                                .reduce( function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0 );
                        
                                // Update footer
                                $( api.column( value ).footer() ).html(
                                    addCommas(total,1)	
                                //pageTotal +'of <br>('+ total +')'
                                );		 
                            })
            
                    } 	
            })
            
            $("#searchResult").click(function(){
                
                drawTitle("Receipts", "", "")
                if (checkDates()) {
                    startDate =  $("#startDate").val()
                    endDate =  $("#endDate").val()
                    url = "app/formProcessor/receiptReportAJAXprocessor.php?startDate="+startDate+"&endDate="+endDate
                    console.log(url)
                    reportTable.ajax.url(url).load();
                }
                
            })
        </script>
        <?php
        break;

    case "clientInfo":
        ?>
        <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
        <script src="assets/extra-libs/DataTables/datatables.min.js"></script>
        <script src="dist/js/reports.js"></script>
        
        <script>
            
                
            clientInfoTable = $("#clientInfoTable").DataTable()
            
            $("#searchResult").click(function(){
                
                phoneNumber=$("#phoneNumber").val()
                $("#reportHeading").html("Search Result for "+phoneNumber)
                
                if (phoneNumber.length == 0 || checkIfPhoneNumber(phoneNumber)) {
                
                    url = "app/formProcessor/receiptClientInfoAJAXprocessor.php?msisdn="+phoneNumber
                    console.log(url)
                    clientInfoTable.ajax.url(url).load();
                }
                
            })
        </script>
        <?php
        break;
    case "message":
        ?>
            
        <link rel="stylesheet" type="text/css" href="assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <script src="assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
        <script src="assets/extra-libs/DataTables/datatables.min.js"></script>
        <script src="dist/js/reports.js"></script>
            
        <script>
        
            jQuery('.dates').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
        
            reportTable = $("#messagesTable").DataTable()
        

            $("#searchResult").click(function(){
                
                drawTitle("Invoices", $("#invoiceStatus").val(), "")
                if (checkDates()) {
                    startDate =  $("#startDate").val()
                    endDate =  $("#endDate").val()
                    url = "app/formProcessor/messageReportAJAXprocessor.php?startDate="+startDate+"&endDate="+endDate
                    console.log(url)
                    reportTable.ajax.url(url).load();
                }
                
            })
            function sendthismessage(here) {

                row = here.closest("tr")
                message = row.find("td:nth-child(2)").text()
                msisdn = row.find("td:nth-child(1)").text()
                $("#messageShow").text(message)
                $("#msisdnNumber").text(msisdn)
                $("#resendMessage").modal("toggle")
               
               
            }

            function resendMessage(button){
                button.attr("disabled", "true")
                message = $("#messageShow").text()
                msisdn = $("#msisdnNumber").text()
                $.ajax({
                    url : 'app/formProcessor/messageResendtAJAXprocessor.php',
                    type : 'get',
                    data : {
                        message : message,
                        msisdn : msisdn
                    },
                    success : function (data) {
                        $("#resendMessage").modal("toggle")
                        toastr.success(
                        'Message Fired!',
                        'Success!')
                        button.removeAttr("disabled")                
                    
                        console.log(url)
                        reportTable.ajax.url(url).load();
                        // drawPickUpPointTable(data)
                    }
                }) 
                
            }
        </script>
        <?php
        break;
    case "request":
        ?>
            
        <link rel="stylesheet" type="text/css" href="assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <script src="assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <link href="assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
        <script src="assets/extra-libs/DataTables/datatables.min.js"></script>
        <script src="dist/js/reports.js"></script>
            
        <script>
        
            jQuery('.dates').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
        
            reportTable = $("#requestTable").DataTable()
        
            $("#searchResult").click(function(){
                
                drawTitle("Requests", "", "")
                if (checkDates()) {
                    startDate =  $("#startDate").val()
                    endDate =  $("#endDate").val()
                   
                    url = "app/formProcessor/requestReportAJAXprocessor.php?startDate="+startDate+"&endDate="+endDate
                    console.log(url)
                    reportTable.ajax.url(url).load();
                }
                
            })
        </script>
        <?php
        break;
    
    case "":

            ?>
            <!-- Charts js Files -->
            <script src="assets/libs/flot/excanvas.js"></script>
            <script src="assets/libs/flot/jquery.flot.js"></script>
            <script src="assets/libs/flot/jquery.flot.pie.js"></script>
            <script src="assets/libs/flot/jquery.flot.time.js"></script>
            <script src="assets/libs/flot/jquery.flot.stack.js"></script>
            <script src="assets/libs/flot/jquery.flot.crosshair.js"></script>
            <script src="assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
            <script src="dist/js/pages/chart/chart-page-init.js"></script>
        
        <?php
        break;
}
?>
<script>
function loadLoader(divId) {
    $('#'+divId).html('<div class="row"><div class="col-md-5"></div><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>')
}

function killLoader() {
    
}
</script>
</body>

</html>
