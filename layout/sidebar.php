
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                    <li class="sidebar-item <?php echo ($currentPage == 'dashboard')?'selected':''; ?>"> 
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a>
                        </li>
                        <li class="sidebar-item <?php echo ($currentPage == 'activeBuses')?'selected':''; ?>"> 
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="activeBuses.php" aria-expanded="false"><i class="mdi mdi-bus"></i><span class="hide-menu">Active Buses</span></a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Client Management</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item <?php echo ($currentPage == 'clientInfo')?'selected':''; ?>"> 
                                    <a href="clientInfo.php" class="sidebar-link"><i class="mdi mdi-account"></i><span class="hide-menu"> Client Info </span></a>
                                </li>
                            </ul>
                        </li>
                        <?php if ($_SESSION["cheetah"]["level"] == 'admin') {?>
                        
                       
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Reports</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item <?php echo ($currentPage == 'invoices')?'selected':''; ?>"> 
                                    <a href="invoices.php" class="sidebar-link"><i class="mdi mdi-bulletin-board"></i><span class="hide-menu"> Invoices </span></a>
                                </li>
                                <li class="sidebar-item <?php echo ($currentPage == 'Receipts')?'selected':''; ?>">
                                    <a href="receipts.php" class="sidebar-link"><i class="mdi mdi-receipt"></i><span class="hide-menu"> Receipts </span></a>
                                </li>
                                <li class="sidebar-item <?php echo ($currentPage == 'message')?'selected':''; ?>"> 
                                    <a href="messages.php" class="sidebar-link"><i class="mdi mdi-email"></i><span class="hide-menu"> Messages </span></a>
                                </li>
                                <?php if ($_SESSION["cheetah"]["addService"] == 'true') {?>
                                <li class="sidebar-item <?php echo ($currentPage == 'Request')?'selected':''; ?>">
                                    <a href="requests.php" class="sidebar-link"><i class="mdi mdi-receipt"></i><span class="hide-menu"> Request </span></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Settings</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item <?php echo ($currentPage == 'Schools')?'selected':''; ?>"> 
                                    <a href="schools.php" class="sidebar-link"><i class="mdi mdi-school"></i><span class="hide-menu"> Schools </span></a>
                                </li>
                                <li class="sidebar-item <?php echo ($currentPage == 'Destination')?'selected':''; ?>">
                                    <a href="destination.php" class="sidebar-link"><i class="mdi mdi-bus"></i><span class="hide-menu"> Destination </span></a>
                                </li>
                                <li class="sidebar-item <?php echo ($currentPage == 'Agents')?'selected':''; ?>">
                                    <a href="agents.php" class="sidebar-link"><i class="mdi mdi-account"></i><span class="hide-menu"> Agents </span></a>
                                </li>
                                <?php if ($_SESSION["cheetah"]["addService"] == 'true') {?>
                        
                                <!-- <li class="sidebar-item <?php echo ($currentPage == 'Service')?'selected':''; ?>">
                                    <a href="service.php" class="sidebar-link"><i class="mdi mdi-settings"></i><span class="hide-menu"> Services </span></a>
                                </li> -->

                                <?php } ?>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-bus"></i><span class="hide-menu">Transport Session</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item <?php echo ($currentPage == 'newSession')?'selected':''; ?>"> 
                                    <a href="newSession.php" class="sidebar-link"><i class="mdi mdi-school"></i><span class="hide-menu"> New Session </span></a>
                                </li>
                                <li class="sidebar-item <?php echo ($currentPage == 'transportSession')?'selected':''; ?>">
                                    <a href="transportSession.php" class="sidebar-link"><i class="mdi mdi-bus"></i><span class="hide-menu"> Sessions </span></a>
                                </li>
                            </ul>
                        </li>
                        <?php } ?>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
    