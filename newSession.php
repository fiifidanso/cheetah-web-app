<?php 

$currentPage = "newSession";
require_once "layout/header.php";
require_once "layout/sidebar.php";
$school = new Cheetah\Models\SchoolModel;
$busTypes = new Cheetah\Models\BusTypeModel;


?>
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">New Transport Session</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">New Transport Session</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
           
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body">
                    <form id="transportSessionForm" class="form-horizontal" action="app/formProcessor/transportSessionProcessor.php" method="post">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                         
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Session Name:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" readonly id="sessionName"
                                        name="sessionName" placeholder="Session Name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">School:</label>
                                    <div class="col-sm-9">
                                        <select required class="select2 form-control custom-select" name="schoolSelect" id="schoolSelect" style="width: 100%; height:36px;">
                                            <option disabled value="0" selected>SELECT SCHOOL</option> 
                                            <?php 
                                            $result = $school->getActiveSchoolQuery();
                                            while ($data = $result->fetch_assoc()) {
                                                echo '<option value="'.$data['id'].'">'.$data['name'].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Destination:</label>
                                    <div class="col-sm-9">
                                        <select required disabled class="select2 form-control custom-select" name="destinationSelect" id="destinationSelect" style="width: 100%; height:36px;">
                                        </select>
                                    </div>
                                </div>
                              
                                <input name="departureDates" id="allDates" type="hidden" value="0"/>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Departure Date</th>
                                            <th>Departure Time</th>
                                        </tr>
                                    </thead>
                                    <tbody id="depatureDatesTable">
                                        <tr>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <input required type="date" class="form-control depatureDates"  class="date">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a  style="color: #00A269 " onclick="addDateRow()">
                                                            <i class="m-r-10 mdi mdi-plus  info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add"></i> 
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="row" >
                                                    <div class="col-md-10"> 
                                                        <input required type="time" class="form-control depatureTime"  class="date">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a  style="color: #00A269 " onclick="addTimeField($(this))">
                                                            <i class="m-r-10 mdi mdi-plus  info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add"></i> 
                                                        </a>
                                                    </div>
                                                </div>
                                                
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <input name="selectedBuses" id="selectedBuses" type="hidden" value="0"/>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Active</th>
                                            <th scope="col">Bus</th>
                                            <th scope="col">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $busTypes->drawTable(); ?>
                                        
                                    </tbody>
                                </table>
                                
                               
                                <div class="modal-footer" >
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                        
                </div>
            </div>
        </div>
           
        </div>
    </div>
</div>


<?php 
require_once "layout/footer.php";
?>