<?php 

$currentPage = "Schools";
require_once "layout/header.php";
require_once "layout/sidebar.php";
$schools = new Cheetah\Models\SchoolModel;
$destination = new Cheetah\Models\DestinationModel;

?>
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Schools</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Schools</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
           
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">New School</h5>
                        <form class="form-horizontal" action="app/formProcessor/schoolProcessor.php" method="post">
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">School Name:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" required id="schoolName" name="schoolName" placeholder="School Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Location:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" required id="schoolLocation" name="schoolLocation" placeholder="School Location">
                                </div>
                            </div>
                            <div class="form-group row">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">All Schools</h5>
                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Location</th>
                                <th scope="col">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $result = $schools->drawTable(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editSchool" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
 aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">School</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editschool" class="form-horizontal" action="app/formProcessor/schoolEditProcessor.php" method="post">
                    <div class="form-group row">
                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">School Name:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="editSchoolName" name="editSchoolName" placeholder="School Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Location:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="editSchoolLocation" name="editSchoolLocation" placeholder="School Location">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 text-right control-label col-form-label">Destination:</label>
                        <div class="col-md-9">
                            <?php 
                                $destination->drawCheckBoxes();
                            ?>
                        </div>
                    </div>
                    <input name="editId" id="editId" type="hidden"/>
                    <input type="hidden" name="method" value="edit"/>
                </form>
                <input type="hidden" id="currentschoolid"/>
                <div id="pickuppointid">
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">Pickup Point</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody id="pickuppointbody" >
                            <tr>
                                <td>
                                    <input type="text"/>
                                </td>
                                <td>
                                    <a   href='#' style='color: #00A269 ' onclick='savePickupRow($(this))'>
                                        <i class='m-r-10 mdi mdi-check danger' data-toggle='tooltip' data-placement='top'
                                        title='' data-original-title='Save'></i>
                                    </a>
                                    <a   href='#' style='color: #ff294d'>
                                        <i class='m-r-10 mdi mdi-close danger' data-toggle='tooltip' data-placement='top'
                                        title='' data-original-title='Delete'></i>
                                    </a>
                                    <a  onclick="addNewPickupRow()" href='#' style='color: #0088cc'>
                                        <i class='m-r-10 mdi mdi-plus danger' data-toggle='tooltip' data-placement='top' 
                                        title='' data-original-title='New Row'></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="$('#editschool').submit()">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteSchool" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete School</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            Are you sure you want to delete  <i id="schoolDeleteName" style="color: #ff294d;">school</i>?     
            <form id="deleteschool" method="post" action="app/formProcessor/schoolEditProcessor.php">
                <input name="deleteId" id="deleteId" type="hidden"/>
                <input type="hidden" name="method" value="delete"/>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" onclick="$('#deleteschool').submit()">Delete</button>
                </div>
            </div>
        </div>
</div>



<?php 
require_once "layout/footer.php";
?>