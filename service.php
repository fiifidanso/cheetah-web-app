<?php 

$currentPage = "Service";
require_once "layout/header.php";
require_once "layout/sidebar.php";
$services = new \Cheetah\Models\BusTypeModel;
$destination = new Cheetah\Models\DestinationModel;

?>
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Service</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Service</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
           
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">New Service</h5>
                        <form class="form-horizontal" action="app/formProcessor/serviceProcessor.php" method="post">
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Service Name:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" required id="serviceName" name="serviceName" placeholder="Service Name">
                                </div>
                            </div>
                                                     
                            <div class="form-group row">
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">All Services</h5>
                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php $services->drawHaulageTable() ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editService" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
 aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Service</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editServiceForm" class="form-horizontal" action="app/formProcessor/serviceEditProcessor.php" method="post">
                    <div class="form-group row">
                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Service Name:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="editServiceName" name="editServiceName" placeholder="School Name">
                        </div>
                    </div>
                   
                   
                    <input name="editServiceId" id="editServiceId" type="hidden"/>
                    <input type="hidden" name="method" value="edit"/>
                </form>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="$('#editServiceForm').submit()">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteService" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Service</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            Are you sure you want to delete  <i id="agentDeleteName" style="color: #ff294d;">school</i>?     
            <form  id="deleteServiceForm" method="post" action="app/formProcessor/serviceEditProcessor.php">
                <input name="deleteId" id="deleteId" type="hidden"/>
                <input type="hidden" name="method" value="delete"/>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" onclick="$('#deleteServiceForm').submit()">Delete</button>
                </div>
            </div>
        </div>
</div>



<?php 
require_once "layout/footer.php";
?>