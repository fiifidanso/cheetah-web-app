<?php 
require_once "app/start.php";

$string = "Select a destination:\n1.Kumasi\n2.Takoradi\n3.Cape Coast\n4.Winneba\n5.Koforidua\n6.Obuasi\n7.Tarkwa\n8.Tamale".
    "\n9.Bolgatanga\n10.Sunyani\n11.Ho\n12.Hohoe\n13.Mamfe\n5.Koforidua\n6.Obuasi\n7.Tarkwa\n8.Tamale\n1.Kumasi\n2.Takoradi\n3.Cape Coast\n4.Winneba";

// checkUSSDLength($string);

function isUSSDLong($message)
{
    $newMessageStructure = [];

    if (strlen($message) > 119) {
        
        $messageChunks = explode("\n", $message);
        $messageString = "";
        $appendMessage= "\n* go back";
        $appendMessage.= "\n# for more";

        foreach ($messageChunks as $key => $message) {
           // the 2 is for \n added to push menu to next line
            if ((strlen($messageString) + 2 + strlen($message) + strlen($appendMessage)) < 119 ) {
                $messageString .= $message."\n";
            } else {
                if (count($newMessageStructure)) {
                       $messageString .= $appendMessage;
                    
                } else {
                    $messageString .= "\n# for more";
                }
               
                array_push($newMessageStructure, $messageString);
                $messageString = '';
            }
            
                
        }

        if (strlen($messageString)) {
            $messageString .= "\n* go back";
            array_push($newMessageStructure, $messageString);
        }

        return true;
    }

    return false;
}

// var_dump(preg_match("[0-23]:[0-59]", ""));

$re = '/[0-23]:[0-59]/m';
$str = '23:59';

preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);

// Print the entire match result
var_dump($matches);
?>