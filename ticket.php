<?php 

require_once "app/start.php";

if (isset($_POST["mobileNumber"]) and isset($_POST["pin"])) {
    $security = new \Cheetah\System\Security;
    
    $security->loginClient($_POST["mobileNumber"], $_POST["pin"]);
    $fail = true;

}

?>
<!DOCTYPE html>
<html dir="ltr">

<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/icon.png">
    <title>Transport App</title>
     <!-- Custom CSS -->
    <link href="dist/css/style.min.css" rel="stylesheet">
    <link href="assets/libs/toastr/build/toastr.min.css" rel="stylesheet">
    <script src="assets/libs/webpmodernizer/modernizr-custom.js"></script>
       
   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div class="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
         <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center">
            <div class="auth-box  border-top border-secondary">
                <div id="loginform">
                    <div class="text-center  p-b-20">
                        <h3> Ticket Login </h3>
                        <!-- <picture>
                            <source srcset="assets/images/logo-text.webp" type="image/webp" style="width: 240px;height: 60px;"> 
                           <source srcset="assets/images/logo-text.jpeg" type="image/jpeg" style="width: 240px;height: 60px;"> 
                            <img src="assets/images/logo-text.webp" alt="Home" class="dark-logo" style="width: 240px;height: 60px;">
                        </picture> -->
                        
                    </div>
                    <!-- Form -->
                    <form class="form-horizontal" action="" method="post">
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Mobile Number:</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" required=""
                                    <?php 
                                    if (isset($_POST["mobileNumber"])) {
                                        echo 'value="'.$_POST["mobileNumber"].'"';
                                    }
                                    ?>
                                    id="mobileNumber" name="mobileNumber" placeholder="Mobile Number">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Pin:</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" required="" id="pin" name="pin" placeholder="Pin">
                                </div>
                            </div>
                             
                            <div class="row border-top border-secondary">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="p-t-20">
                                        <!-- <button class="btn btn-info" id="to-recover" type="button"><i class="fa fa-lock m-r-5"></i> Lost password?</button> -->
                                        <button class="btn btn-success float-right" type="submit">Login</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                </div>
               
            </div>
        </div>
     
    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/libs/toastr/build/toastr.min.js"></script>
        
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->
 

    <script>
            <?php 
                if (isset($fail) and $fail) {
            ?>
            $(document).ready(toastr.error(
                'Incorrect number/pin',
                    'Error!')
                )
            <?php }?>
    </script>

</body>

</html>