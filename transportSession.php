<?php 

$currentPage = "transportSession";
require_once "layout/header.php";
require_once "layout/sidebar.php";
$transportSession = new Cheetah\Models\TransportSessionModel;
$busTypes = new Cheetah\Models\BusTypeModel;


?>
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Transport Sessions</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Transport Sessions</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->



    
    <div class="container-fluid">
           
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body">
                    <form id="transportSessionForm" class="form-horizontal" action="app/formProcessor/transportSessionProcessor.php" method="post">
                        <div class="row">
                        <table id="transportTable" class="table">
                                <thead>
                                    <tr>
                                    <th scope="col">Session Name</th>
                                    <th scope="col">Departure Dates</th>
                                    <th scope="col">Buses</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        $transportSession->drawTransportSessionTable();
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
           
    </div>
</div>


<div class="modal fade" id="editTransportSession" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
 aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Transport Session:</h5> <h4 style="margin-left: 10px;" id="editSessionName"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form id="editTransportSessionForm" method="POST" action="app/formProcessor/transportSessionEditProcessor.php">
            <input type="hidden" name="method" value="edit"/>
            <input type="hidden" id="editId" name="editId"/>
            <input type="hidden" id="selectedBuses" name="selectedBuses" />
            
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group row">
                                <div class="col-md-1">
                                    <label for="fname" class="text-right control-label col-form-label">Status:</label>
                                </div>
                                <div class="col-md-9" style="padding-top: 5px;">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="pendingRadioButton" name="status" value="pending" required>
                                                <label class="custom-control-label" for="pendingRadioButton">Pending</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="activeRadioButton" name="status" value="active" required>
                                                <label class="custom-control-label" for="activeRadioButton">Active</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" id="pausedRadioButon" name="status" value="paused"  required>
                                                <label class="custom-control-label" for="pausedRadioButon">Pause</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#depDateTab" role="tab" aria-selected="false"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Departure Dates</span></a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#busTypeTab" role="tab" aria-selected="false"><span class="hidden-sm-up"></span> <span class="hidden-xs-down">Bus Types</span></a> </li>
                    </ul>
                    <div class="tab-content tabcontent-border">
                        <div class="tab-pane active show" id="depDateTab" role="tabpanel">
                            <div class="p-10">
                                <input id="allEditDates" name="allEditDates" type="hidden"/>
                                
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Departure Date</th>
                                            <th>Departure Time</th>
                                        </tr>
                                    </thead>
                                    <tbody id="depatureDatesTable">
                                        <tr>
                                            <td>
                                                <div class="lds-roller">
                                                <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane " id="busTypeTab" role="tabpanel">
                            <div class="p-10">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th scope="col">Active</th>
                                        <th scope="col">Bus</th>
                                        <th scope="col">Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $busTypes->drawTable(); ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                       
                        
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="deleteTransportSession" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Transport Session</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            Are you sure you want to delete  <i id="deleteName" style="color: #ff294d;">school</i>?     
            <form id="deleteTransportForm" method="post" action="app/formProcessor/transportSessionEditProcessor.php">
                <input name="deleteId" id="deleteId" type="hidden"/>
                <input type="hidden" name="method" value="delete"/>
            </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" onclick="$('#deleteTransportForm').submit()" >Delete</button>
                </div>
            </div>
        </div>
</div>

<?php 
require_once "layout/footer.php";
?>