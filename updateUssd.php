<?php 

require_once "app/start.php";

$databases = [];

$schools = new Cheetah\Models\SchoolModel;
$transportSession = new Cheetah\Models\TransportSessionModel;
$pickupPoints = new Cheetah\Models\PickupPointsModel;
$agents = new Cheetah\Models\AgentModel;
$admins = new Cheetah\Models\Admin;

$allCompanies = $admins->getAllCompanies();

$finalDatabase=[];
$companyDatabase = [];

while ($admin = $allCompanies->fetch_assoc()) {
    $schoolsQuery = $schools->getActiveSchoolByCompanyQuery($admin["company"]);
    
    array_push($companyDatabase, [$admin["company"], $admin["name"], $admin["is_haulage"]]);
    
    $databases["schools"] = [];

    while ($data = $schoolsQuery->fetch_assoc()) {
        array_push(
            $databases["schools"],
            [
                "id" => $data['id'],
                "name" => $data['name'],
                "pickupPoints" => [["id"=>1, "name" => "adsa"]]
            ]
        );
    }


    $database["transportSessions"] = [];
    $realDatabase = [];
    foreach ($databases["schools"] as $school) {
        
        $databases["transportSessions"][$school['id']]['name'] = $school['name'];
        $databases["transportSessions"][$school['id']]['id'] = $school['id'];
        $databases["transportSessions"][$school['id']]["pickupPoints"] = $pickupPoints->getAllPoints($school['id']);
        
        $databases["transportSessions"][$school['id']] ['content']= $transportSession->getTransportSessionModelUSSD($school['id']);

        if ((count($databases["transportSessions"][$school['id']] ['content']))) {

            array_push($realDatabase, $databases["transportSessions"][$school['id']]);
        
        }
    }
    $finalDatabase[$admin["company"]] = $realDatabase;
}

file_put_contents("./ussd/database", json_encode($finalDatabase));
echo "<pre>";
print_r($finalDatabase);


echo "<pre>";
print_r($companyDatabase);


$vitalInfo = [];

$vitalInfo['companies'] =$companyDatabase;

$vitalInfo["agents"] = $agents->pullUSSDDatabase();



$vitalInfo["paymentMethods"] = [
    [ 
        "name" => "MTN",
        "type" => "momo"
    ],
    [ 
        "name" => "VODAFONE",
        "type" => "momo"
    ],
    [ 
        "name" => "AIRTEL",
        "type" => "momo"
    ],
    [ 
        "name" => "TIGO",
        "type" => "momo"
    ],
    [ 
        "name" => "CASH",
        "type" => "cash"
    ]
    
];

file_put_contents("./ussd/vitalInfo", json_encode($vitalInfo));


?>