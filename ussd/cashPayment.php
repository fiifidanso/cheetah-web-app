<?php 

require_once __DIR__."/../app/start.php";

$invoiceID = filter_input(INPUT_GET, 'invoiceID');

file_put_contents(
    "cashPayment",
    $invoiceID.PHP_EOL,
    FILE_APPEND
);

successPayment($invoiceID);


function successPayment($order_id) 
{
    
    $payment = new Cheetah\System\Payment($order_id);

    $details = $payment->validatePayment();

    $bus = new Cheetah\System\BusManagement;

    $bus->loadByBusTransportSession(
        $details["invoice"]->pullColumnInfo("transport_bus_id")
    );

    $bus->loadCurrentLoadingBus(
        $details["invoice"]->pullColumnInfo("departure_date_id")
    );

    $bus->assignTicketsToBus($details["invoice"], $details["receipt"]);
    
    $admin = new Cheetah\Models\Admin;

    
    $senderID = $admin->fetch_sender_id($payment->getInvoiceCompany());

    $sendSms = new Cheetah\System\SendSms;

    $message = $payment->createPaymentSMS();
    
    $sendSms->setSenderID($senderID);

    $sendSms->sendSms(
        $message,
        $details["invoice"]->pullColumnInfo("msisdn"),
        $payment->getInvoiceCompany()
    );
    
}

?>