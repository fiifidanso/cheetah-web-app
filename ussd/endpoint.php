<?php 

require_once __DIR__."/../app/start.php";

$incomingJSON = file_get_contents('php://input');

file_put_contents(
    "incomingAPI",
    $incomingJSON.PHP_EOL,
    FILE_APPEND
);

echo successPayment();


function successPayment($order_id) 
{
    
    $payment = new Cheetah\System\Payment($order_id);

    $details = $payment->validatePayment();

    $bus = new Cheetah\System\BusManagement;

    $bus->loadByBusTransportSession(
        $details["invoice"]->pullColumnInfo("transport_bus_id")
    );

    $bus->loadCurrentLoadingBus(
        $details["invoice"]->pullColumnInfo("departure_date_id")
    );

    $bus->assignTicketsToBus($details["invoice"], $details["receipt"]);
    
    $sendSms = new Cheetah\System\SendSms;

    $message = $payment->createPaymentSMS();
    
    $sendSms->sendSms(
        $message,
        $details["invoice"]->pullColumnInfo("msisdn")
    );
    
}

?>