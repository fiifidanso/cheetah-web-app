<?php

function startsession($name, $message)
{   
    $sessionfile = "sessions/" . md5($name);
    $data = [
        "state"=>"start",
        "previousMessage"=>$message
    ];
    file_put_contents($sessionfile, json_encode($data));
}

function startsessionAgent($name, $message, $agent, $companyName, $companyIndex)
{   
    $sessionfile = "sessions/" . md5($name);
    $data = [
        "state"=>"awaitingSchoolSelection",
        "previousMessage"=>$message,
        'agentId' => $agent,
        "selectedCompany"=> $companyName,
        "selectedCompanyIndex" => $companyIndex
    ];
    file_put_contents($sessionfile, json_encode($data));
}


function editsession($name, $key, $data)
{
    $sessionfile = "sessions/" . md5($name);
    $existingdata = json_decode(shell_exec("cat $sessionfile"));
    $existingdata[$key] = $data;
    file_put_contents($sessionfile, json_encode($data));
}


function getdata($name)
{  
    $sessionfile = "sessions/" . md5($name);
    $existingdata = json_decode(shell_exec("cat $sessionfile"), true);
    return $existingdata;    
}

function saveSession($name, $session)
{   
    $sessionfile = "sessions/" . md5($name);
    file_put_contents($sessionfile, json_encode($session));
}


function saveNumberForContact($msisdn, $school)
{
    $query = "INSERT INTO save_users (msisdn, school) VALUES ('$msisdn', '$school')";
    $thisLink = new mysqli(DBHOST, DBUSERNAME, DBPASSWORD, DBNAME);
    $thisLink->query($query);
}


//validate msisdn
function isMsisdn($msisdn)
{
    $msisdn = trim($msisdn);

    if (is_numeric($msisdn) and strlen($msisdn) <= 12) {
        if (substr($msisdn, 0, 1) == '0' and strlen($msisdn) == '10') {
            return '233'.substr($msisdn, 1);
        } elseif (substr($msisdn, 0, 3) == '233' and strlen($msisdn) == '12') {
            return $msisdn;
        }
    }
    return false;
}

function isUSSDLong($message)
{
    $newMessageStructure = [];

    if (strlen($message) > 170) {
        
        $messageChunks = explode("\n", $message);
        $messageString = "";
        //providing space to all possible inputs
        $appendMessage= "\n0 go back\n99 next page";
       
        foreach ($messageChunks as $key => $message) {
          
            if ((strlen($messageString) + 2 + strlen($message) + strlen($appendMessage)) < 169 ) {
                $messageString .= $message."\n";
            } else {
                if (count($newMessageStructure)) {
                       $messageString .= $appendMessage;
                    
                } else {
                    $messageString .= "\n99 next page";
                }
               
                array_push($newMessageStructure, $messageString);
                $messageString = $message."\n";
            }
            
                
        }

        if (strlen($messageString)) {
            $messageString .= "\n0 go back";
            array_push($newMessageStructure, $messageString);
        }
        $GLOBALS["sessionData"]["longUSSDMessages"] = $newMessageStructure;
        return true;
    }

    return false;
}
?>
