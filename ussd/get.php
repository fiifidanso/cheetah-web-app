<?php

date_default_timezone_set("UTC");
require_once "functions.php";
require_once "../app/variables.php";


$sessionData = getdata($_GET["msisdn"]);
sleep(15);
initiateMobileMoney($_GET["invoice_num"]);

function initiateMobileMoney($order_id)
{
    $curl = curl_init();

    $username = MOMO_USERNAME;
    $password = MOMO_PASSWORD;
    $merchantID = MOMO_MERCHANTID;

    $key = rand(1000, 9999);
    $secrete = md5($username . $key . md5($password));

    $momoNetwork = json_decode(file_get_contents("vitalInfo"))
        ->paymentMethods[$GLOBALS["sessionData"]["selectedPaymentMethod"]]
        ->name;

    $momoNumber = $GLOBALS["sessionData"]["momoNumber"];
    $totalAmount = number_format(($GLOBALS["sessionData"]["totalAmount"] / 100), 2);
    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];


    if ($momoNetwork == "VODAFONE") {
        $description = $GLOBALS["sessionData"]["vodafoneVoucher"];
    } else {

        $description = "Cheetah_ticket " . date('Y-m-d H:i:s');
    }

    $arrayStructure = [
        "merchant_id" => $merchantID,
        "secrete" => $secrete,
        "key" => $key,
        "order_id" => $order_id,
        "customerName" => "Cheetah Transport",
        "amount" => $totalAmount,
        "item_desc" => $description,
        "customerNumber" => $momoNumber,
        "payby" => $momoNetwork,
        "callback" => MOMO_CALLBACK
    ];

    $input = json_encode($arrayStructure);
    //MTN, AIRTEL, TIGO, VODAFONE
    curl_setopt_array(
        $curl,
        array(
            CURLOPT_URL => MOMO_API_ENPOINT,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $input,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json"
            ),
        )
    );

    // switch between the test and live data here
        
    $response = curl_exec($curl);
    curl_close($curl);


    file_put_contents(
        "apiCallLog",
        date('Y-m-d H:i:s') . " [INPUT:$input] [OUTPUT:$response]" . PHP_EOL,
        FILE_APPEND
    );

    $query = "INSERT INTO momo 
        (msisdn, network, amount, `status`, order_id, response, `description`, company)
        VALUES (
            '$momoNumber',
            '$momoNetwork',
            '$totalAmount',
            'pending',
            '$order_id',
            '$response',
            '$description',
            '$selectedCompany'
        )";
    insertIntoDatabase($query);

}


function insertIntoDatabase($query)
{
    // Switch between love and localhost

    $dbConnection = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

    if ($dbConnection->connect_errno) {
        echo "Failed to connect to MySQL: " . $dbConnection->connect_error;
    }

    $dbConnection->query($query);

}

?>