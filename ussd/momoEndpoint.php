<?php


require_once __DIR__."/../app/start.php";

$incomingJSON = file_get_contents('php://input');

file_put_contents(
    "momoLog",
    $incomingJSON.PHP_EOL,
    FILE_APPEND
);

$mobileMoneyResolver = new \Cheetah\System\MobileMoney;

$mobileMoneyResolver->processResponse($incomingJSON);


?>