<?php 

require_once __DIR__."/../app/start.php";

$msisdn = filter_input(INPUT_GET, 'msisdn');
$message = filter_input(INPUT_GET, 'message');
$company = filter_input(INPUT_GET, 'company');

$admin = new Cheetah\Models\Admin;

$senderID = $admin->fetch_sender_id($company);

$sendSms = new Cheetah\System\SendSms;


$sendSms->setSenderID($senderID);

$sendSms->sendSms(
    $message,
    $msisdn,
    $company
);
    


?>