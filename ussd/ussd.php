<?php

date_default_timezone_set("UTC");
require_once "functions.php";
require_once "../app/variables.php";
$ussdJSON = file_get_contents('php://input');



$ussdUnqiueUserID = "CHT92027";

$inputUSSD = json_decode($ussdJSON);

/**
 * 
 * inputUSSD contents
 * "USERID": ussdUnqiueUserID
 * "MSISDN": Of the incoming user
 * "USERDATA": Input data from user
 * "MSGTYPE" : State of the session
 */

$outputUSSD = [
    "USERID" => $ussdUnqiueUserID,
    "MSISDN" => $inputUSSD->MSISDN,
    "MSGTYPE" => true,
    "MSG"=>""
];

if ($inputUSSD->MSGTYPE) {

    if (isAgent()) {
        $var = $inputUSSD->MSISDN;

        $name = json_decode(file_get_contents("vitalInfo"))->agents->$var->name;
        $companyInfo =selectAgentCompanyIndex($var);
        $outputUSSD["MSG"] = "Hello $name";
        $outputUSSD["MSG"] .= "\n\nSelect your Location:";
        $outputUSSD["MSG"] .= loadSchoolsAgent($companyInfo["name"]);
        startsessionAgent(
            $inputUSSD->MSISDN,
            $outputUSSD["MSG"],
            json_decode(file_get_contents("vitalInfo"))->agents->$var->id,
            $companyInfo["name"],
            $companyInfo["index"]
        );

    } else {
        $outputUSSD["MSG"] = "Welcome to your #1 Digital Transport Platform";
        $outputUSSD["MSG"] .= "\nSelect your service provider.";
        $outputUSSD["MSG"] .= loadCompanies();
        startsession($inputUSSD->MSISDN, $outputUSSD["MSG"]);

        // $outputUSSD["MSG"] = "Welcome to Cheetah Transport Services";
        // $outputUSSD["MSG"] .= "\nYour #1 student transport services provider.";
        // $outputUSSD["MSG"] .= "\n\nSelect your Location:";
        // $outputUSSD["MSG"] .= loadSchools();
        // startsession($inputUSSD->MSISDN, $outputUSSD["MSG"]);
    
    }


} else {

    $sessionData = getdata($inputUSSD->MSISDN);
    
    //Used to check if the incoming message is longer than what is required
    if ($sessionData['state'] == 'longUSSDMessage') {

        if ($inputUSSD->USERDATA == "0" || $inputUSSD->USERDATA == "99") {
            if (!sortLongUSSdMessage()) {
                $sessionData['state'] = $sessionData['resumeState'];
            }
        } else {
            $sessionData['state'] = $sessionData['resumeState'];
        }
    
    }

    switch ($sessionData['state']) {
        case 'start':
            selectCompanyLocation();
            break;
        case 'awaitingSchoolSelection':
            selectSchoolDestination();
            break;
        case 'awaitingDestinationInput':
            selectBusDate();
            break;
        case 'awaitingDepartureDateSelection':
            selectPickUpTime();
            break;
        case 'awaitingPickupTimeSelection':
            selectPickUpPoint();
            break;
        case 'awaitingPickupPointSelection':
            isSelectedCompanyHaulage();
            break;
        case 'awaitingTicketQuantity':
            selectPaymentMethod();
            break;
        case 'awaitingSelectedService':
            selectPaymentMethodHaulageService();
            break;
        case 'awaitingMomoNetwork':
            inputMOMONumber();
            break;
        case 'vodafoneVoucher':
            vodafoneVoucher();
            break;
        case 'finalConfirmation':
            showFinalConfirmationScreen();
            break;
        case 'finalStage':
            finalStage();
            break;
        case 'awaitingHaulageServiceSelection':
            showHaulage();
            break;
        case 'awaitingHaulageConfirmation':
            finalStageHaulage();
            break;
    

    }

    if (isUSSDLong($outputUSSD["MSG"])) {
        $sessionData["resumeState"] = $sessionData['state'];
        $sessionData['state'] = 'longUSSDMessage';
        $sessionData['longMessageIndex'] = 0;
        $outputUSSD["MSG"] = $sessionData["longUSSDMessages"][0];
        
    } 


    saveSession($inputUSSD->MSISDN, $sessionData);
}

// $outputUSSD["MSG"] .= " ".strlen($outputUSSD["MSG"]);
header('Content-Type: application/json;charset=utf-8');

echo json_encode($outputUSSD);

file_put_contents(
    "ussdLog",
    date("Y-m-d H:i:s") . "[INPUT: $ussdJSON] [OUTPUT:" . json_encode($outputUSSD) . " ]" . PHP_EOL,
    FILE_APPEND
);



function isAgent()
{
    $var = $GLOBALS["inputUSSD"]->MSISDN;
    return isset(json_decode(file_get_contents("vitalInfo"))->agents->$var);
}


function sortLongUSSdMessage()
{
    if ($GLOBALS['inputUSSD']->USERDATA == "0") {
        $key = $GLOBALS["sessionData"]['longMessageIndex'] - 1;
    } else {
        $key = $GLOBALS["sessionData"]['longMessageIndex'] + 1;
    }

    if (array_key_exists($key, $GLOBALS["sessionData"]["longUSSDMessages"])) {
        $GLOBALS['outputUSSD']["MSG"] = $GLOBALS["sessionData"]["longUSSDMessages"][$key];
        $GLOBALS["sessionData"]['longMessageIndex'] = $key;
        return true;
    }
    return false;
}








function selectCompanyLocation()
{

    
    if (companySelectedValid()) {

        $GLOBALS['outputUSSD']["MSG"] .= "Select your Location:";
        $GLOBALS['outputUSSD']["MSG"] .= loadSchools();
        
        
        $GLOBALS["sessionData"]["state"] = "awaitingSchoolSelection";
        $GLOBALS["sessionData"]["previousMessage"] = $GLOBALS['outputUSSD']["MSG"];
    } else {

        wrongInput();
    }


}


function selectSchoolDestination()
{


    if (schoolSelectedValid()) {

        $GLOBALS['outputUSSD']["MSG"] = "Select your destination:";
        $GLOBALS['outputUSSD']["MSG"] .= loadSelectedSchoolDestination();
        // $GLOBALS['outputUSSD']["MSG"] .= "\n0. decline";

        $GLOBALS["sessionData"]["state"] = "awaitingDestinationInput";
        $GLOBALS["sessionData"]["previousMessage"] = $GLOBALS['outputUSSD']["MSG"];
    } else {

        wrongInput();
    }


}



function selectBusDate()
{
    if (destinationValid()) {
        $GLOBALS['outputUSSD']["MSG"] = "Select departure date:";
        $GLOBALS['outputUSSD']["MSG"] .= loadSelectedSchoolDestinationDepartureDates();
        // $GLOBALS['outputUSSD']["MSG"] .= "\n0. decline";

        $GLOBALS["sessionData"]["state"] = "awaitingDepartureDateSelection";
        $GLOBALS["sessionData"]["previousMessage"] = $GLOBALS['outputUSSD']["MSG"];
    } else {
        wrongInput();
    }
}


function selectPickUpTime()
{
    if (departueDateValid()) {
        $GLOBALS['outputUSSD']["MSG"] = "Select pickup time:";
        $GLOBALS['outputUSSD']["MSG"] .= loadSelectedDateDepartureTime();
        // $GLOBALS['outputUSSD']["MSG"] .= "\n0. decline";

        $GLOBALS["sessionData"]["state"] = "awaitingPickupTimeSelection";
        $GLOBALS["sessionData"]["previousMessage"] = $GLOBALS['outputUSSD']["MSG"];
    } else {
        wrongInput();
    }
}

function selectPickUpPoint()
{
    if (pickUpTimeValid()) {
        $GLOBALS['outputUSSD']["MSG"] = "Select pickup point:";
        $GLOBALS['outputUSSD']["MSG"] .= loadSelectedSchoolPickupPoints();
        // $GLOBALS['outputUSSD']["MSG"] .= "\n0. decline";

        $GLOBALS["sessionData"]["state"] = "awaitingPickupPointSelection";
        $GLOBALS["sessionData"]["previousMessage"] = $GLOBALS['outputUSSD']["MSG"];
    } else {
        wrongInput();
    }
}


function isSelectedCompanyHaulage()
{
    // Select selectd company
    $selectedCompany = json_decode(file_get_contents("vitalInfo"))
        ->companies[$GLOBALS["sessionData"]["selectedCompanyIndex"]];
    
    // Checking to see if comapny is a haulage Company
    
    if ($selectedCompany[2] == "true") {
        showHualageOption();
    } else {
        showConfirmStage1();
    }
  
  
}

function showConfirmStage1()
{
    if (pickupPointValid()) {
        $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];
  

        $GLOBALS['outputUSSD']["MSG"] = json_decode(file_get_contents("database"))
            ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
            ->name;
        $GLOBALS['outputUSSD']["MSG"] .= " to ";

        $GLOBALS['outputUSSD']["MSG"] .= json_decode(file_get_contents("database"))
            ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
            ->content[$GLOBALS["sessionData"]["selectedDestination"]]
            ->name;


        $currentDate = $GLOBALS["sessionData"]["availableDates"][$GLOBALS["sessionData"]["selectedDepartureDate"]];
        $currentDate = new DateTime($currentDate);

        $selectedTime =  $GLOBALS["sessionData"]["availableTime"][$GLOBALS["sessionData"]["selectedDepartureTime"]]["time"];
        $GLOBALS['outputUSSD']["MSG"] .= "\n" . $currentDate->format('d M Y').' @ '.$selectedTime;




        $GLOBALS['outputUSSD']["MSG"] .= "\nPickup:" . json_decode(file_get_contents("database"))
            ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
            ->pickupPoints[$GLOBALS["sessionData"]["selectedPickupPoint"]]
            ->name;

        $GLOBALS['outputUSSD']["MSG"] .= "\nPrice:GHS" . number_format(
            json_decode(file_get_contents("database"))
                ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
                ->content[$GLOBALS["sessionData"]["selectedDestination"]]
                ->busses[0]
                ->amount / 100,
            2
        );

        $GLOBALS['outputUSSD']["MSG"] .= "\nInput no of tickets to buy:";

        $GLOBALS["sessionData"]["state"] = "awaitingTicketQuantity";
        $GLOBALS["sessionData"]["previousMessage"] = $GLOBALS['outputUSSD']["MSG"];
    } else {
        wrongInput();
    }
}


function showHualageOption()
{
    if (pickupPointValid()) {
      
        // Give breakdown summarry of details
        
        $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];
        
        $school = json_decode(file_get_contents("database"))
            ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
            ->name;
        $GLOBALS['outputUSSD']["MSG"] =  $school;
        $GLOBALS['outputUSSD']["MSG"] .= " to ";

        $destination = json_decode(file_get_contents("database"))
            ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
            ->content[$GLOBALS["sessionData"]["selectedDestination"]]
            ->name;

        $GLOBALS['outputUSSD']["MSG"] .= $destination;
        
        $currentDate = $GLOBALS["sessionData"]["availableDates"][$GLOBALS["sessionData"]["selectedDepartureDate"]];
        $currentDate = new DateTime($currentDate["date"]);

        $departureDate =  $currentDate->format('d M Y');
        
        $GLOBALS['outputUSSD']["MSG"] .= "\n" .$departureDate;
        
        $pickupPoint =  json_decode(file_get_contents("database"))
            ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
            ->pickupPoints[$GLOBALS["sessionData"]["selectedPickupPoint"]]
            ->name;

        $GLOBALS['outputUSSD']["MSG"] .= "\n" .$pickupPoint;
           
        
                
               
        $GLOBALS['outputUSSD']["MSG"] .= "\n1.Confirm Request";
        

        $GLOBALS["sessionData"]["sendMessage"] = "Dear Client your request has been received,".
        "School:$school".
        " Destination:$destination".
        " Pickup:$pickupPoint".
        " Date:$departureDate";
        
       
        $GLOBALS["sessionData"]["state"] = "awaitingHaulageConfirmation";
        $GLOBALS["sessionData"]["previousMessage"] = $GLOBALS['outputUSSD']["MSG"];
    } else {
        wrongInput();
    }
}




function finalStageHaulage()
{
    if ($GLOBALS['inputUSSD']->USERDATA == '1') {
        $companyName =  json_decode(file_get_contents("vitalInfo"))
        ->companies[$GLOBALS["sessionData"]["selectedCompanyIndex"]][1];
  
  
        $GLOBALS['outputUSSD']["MSG"] = "Your request has been successfully received.";
        $GLOBALS['outputUSSD']["MSG"] .= "\n An agent would be in contact with you soon.";
        $GLOBALS['outputUSSD']["MSG"] .= "Thank you for using ".$companyName;
        

        $GLOBALS['outputUSSD']["MSGTYPE"] = false;

        createHaulageRequester();
    } else {
        wrongInput();
    }
}

function selectPaymentMethod()
{
    if (ticketNumberValid()) {
        //Check for agent Level things here {}
        $GLOBALS['outputUSSD']["MSG"] = "Select your mobile money wallet:";

        if (isset($GLOBALS["sessionData"]["agentId"])) {
            $GLOBALS['outputUSSD']["MSG"] = "Select your Payment Method:";
        }
        $GLOBALS['outputUSSD']["MSG"] .= loadMOMOOperator();

        $GLOBALS["sessionData"]["state"] = "awaitingMomoNetwork";
        $GLOBALS["sessionData"]["previousMessage"] = $GLOBALS['outputUSSD']["MSG"];

    } else {
        wrongInput();
    }
}

function selectPaymentMethodHaulageService()
{
    if (serviceSelectionValid()) {
        //Check for agent Level things here {}
        $GLOBALS['outputUSSD']["MSG"] = "Select your MOMO operator:";

        if (isset($GLOBALS["sessionData"]["agentId"])) {
            $GLOBALS['outputUSSD']["MSG"] = "Select your Payment Method:";
        }
        $GLOBALS['outputUSSD']["MSG"] .= loadMOMOOperator();

        $GLOBALS["sessionData"]["state"] = "awaitingMomoNetwork";
        $GLOBALS["sessionData"]["previousMessage"] = $GLOBALS['outputUSSD']["MSG"];

    } else {
        wrongInput();
    }
}

function inputMOMONumber()
{

    if (validMomoNetwork()) {

        $momoNetwork = json_decode(file_get_contents("vitalInfo"))
            ->paymentMethods[$GLOBALS["sessionData"]["selectedPaymentMethod"]]
            ->name;

        $GLOBALS["sessionData"]["state"] = "finalConfirmation";

        if ($momoNetwork == "MTN") {
            $GLOBALS['outputUSSD']["MSG"] = "Input your MTN MOMO number:";
        } elseif ($momoNetwork == "VODAFONE") {

            $GLOBALS['outputUSSD']["MSG"] = "Dail *110# on your VODAFONE CASH phone";
            $GLOBALS['outputUSSD']["MSG"] .= "\nPress 6 Generate Voucher";
            $GLOBALS['outputUSSD']["MSG"] .= "\nEnter PIN to generate voucher";
            $GLOBALS['outputUSSD']["MSG"] .= "\n\nInput voucher code provided below";

            $GLOBALS["sessionData"]["state"] = "vodafoneVoucher";

        } elseif ($momoNetwork == "AIRTEL") {
            $GLOBALS['outputUSSD']["MSG"] = "Input your AIRTEL MONEY number:";
        } elseif ($momoNetwork == "TIGO") {
            $GLOBALS['outputUSSD']["MSG"] = "Input your TIGO CASH number:";
        }



        if (isset($GLOBALS["sessionData"]["cashPayments"])) {
            $GLOBALS['outputUSSD']["MSG"] = "Input client's mobile number:";
        }

        $GLOBALS["sessionData"]["previousMessage"] = $GLOBALS['outputUSSD']["MSG"];

    } else {
        wrongInput();
    }
}

function vodafoneVoucher()
{
    if (validVodafoneVoucher()) {

        $GLOBALS['outputUSSD']["MSG"] = "Input your VODAFONE CASH number:";
        $GLOBALS["sessionData"]["state"] = "finalConfirmation";
        $GLOBALS["sessionData"]["previousMessage"] = $GLOBALS['outputUSSD']["MSG"];

    } else {
        wrongInput();
    }
}


function showFinalConfirmationScreen()
{
    if (validMSISDN()) {

        $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];
  
        $GLOBALS['outputUSSD']["MSG"] = json_decode(file_get_contents("database"))
            ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
            ->name;

        $GLOBALS['outputUSSD']["MSG"] .= " to ";

        $GLOBALS['outputUSSD']["MSG"] .= json_decode(file_get_contents("database"))
            ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
            ->content[$GLOBALS["sessionData"]["selectedDestination"]]
            ->name;

        $currentDate = $GLOBALS["sessionData"]["availableDates"][$GLOBALS["sessionData"]["selectedDepartureDate"]];
        $currentDate = new DateTime($currentDate);
        $selectedTime =  $GLOBALS["sessionData"]["availableTime"][$GLOBALS["sessionData"]["selectedDepartureTime"]]["time"];
        

        $payMethod = json_decode(file_get_contents("vitalInfo"))
            ->paymentMethods[$GLOBALS["sessionData"]["selectedPaymentMethod"]]
            ->name;

        $GLOBALS['outputUSSD']["MSG"] .= "\n" . $currentDate->format('d M Y')." @ ".$selectedTime;

        $GLOBALS['outputUSSD']["MSG"] .= "\n" . json_decode(file_get_contents("database"))->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]->pickupPoints[$GLOBALS["sessionData"]["selectedPickupPoint"]]->name;

        
        $isHaulageCompany = json_decode(file_get_contents("vitalInfo"))
        ->companies[$GLOBALS["sessionData"]["selectedCompanyIndex"]][2];
   
        // Checking to see if comapny is a haulage Company

        if ($isHaulageCompany == "true") {
            
            $GLOBALS['outputUSSD']["MSG"] .= "\n" . $GLOBALS["sessionData"]["serviceName"];
        
        } else {

            $GLOBALS['outputUSSD']["MSG"] .= "\n" . $GLOBALS["sessionData"]["ticketNo"] . " tcks";
            $GLOBALS["sessionData"]["tickets"] = $GLOBALS["sessionData"]["ticketNo"] *
                json_decode(file_get_contents("database"))->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
                ->content[$GLOBALS["sessionData"]["selectedDestination"]]
                ->busses[0]
                ->amount;
        }



        if ($payMethod == "CASH") {
            $GLOBALS["sessionData"]["momo_charge"] = 0;
        } else {
            // Turn this off if its haulage
            if ($isHaulageCompany != "true") {
       
                $GLOBALS["sessionData"]["momo_charge"] = $GLOBALS["sessionData"]["ticketNo"] *
                    json_decode(file_get_contents("database"))->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
                    ->content[$GLOBALS["sessionData"]["selectedDestination"]]
                    ->busses[0]
                    ->momo_charge;
            }
        }

        if ($isHaulageCompany == "true") {
            $GLOBALS['outputUSSD']["MSG"] .= "\n" . number_format(
                $GLOBALS["sessionData"]["tickets"] / 100,
                2
            );
        } else {
            // Turn this off if its haulage
            
            $GLOBALS["sessionData"]["nhil"] = $GLOBALS["sessionData"]["ticketNo"] *
                json_decode(file_get_contents("database"))->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
                ->content[$GLOBALS["sessionData"]["selectedDestination"]]
                ->busses[0]
                ->nhil;

            // Turn this off if its haulage
            
            $GLOBALS["sessionData"]["totalAmount"]
                = $GLOBALS["sessionData"]["tickets"] +
                $GLOBALS["sessionData"]["momo_charge"] +
                $GLOBALS["sessionData"]["nhil"];
            
            $GLOBALS['outputUSSD']["MSG"] .= "\nTckt: " . number_format(
                $GLOBALS["sessionData"]["tickets"] / 100,
                2
            );
        }
       




        if ($GLOBALS["sessionData"]["momo_charge"] > 0) {

            $GLOBALS['outputUSSD']["MSG"] .= "\nMOMO+NHIL+VAT charge: " . number_format(
                ($GLOBALS["sessionData"]["momo_charge"] + $GLOBALS["sessionData"]["nhil"]) / 100,
                2
            );
        } else {
            $GLOBALS['outputUSSD']["MSG"] .= "\nNHIL+VAT charge: " . number_format(
                $GLOBALS["sessionData"]["nhil"] / 100,
                2
            );
        }



        $GLOBALS['outputUSSD']["MSG"] .= "\nTotal:GHS" . number_format(
            $GLOBALS["sessionData"]["totalAmount"] / 100,
            2
        );


        $GLOBALS['outputUSSD']["MSG"] .= "\n1.Confirm";


        $GLOBALS["sessionData"]["state"] = "finalStage";

        $GLOBALS["sessionData"]["previousMessage"] = $GLOBALS['outputUSSD']["MSG"];

    } else {
        wrongInput();
    }
}




function finalStage()
{
    if ($GLOBALS['inputUSSD']->USERDATA == '1') {

        $momoNetwork = json_decode(file_get_contents("vitalInfo"))
            ->paymentMethods[$GLOBALS["sessionData"]["selectedPaymentMethod"]]
            ->name;

        if ($momoNetwork == "MTN") {
            $totalAmount = number_format(($GLOBALS["sessionData"]["totalAmount"]) + (0.80 * $GLOBALS["sessionData"]["ticketNo"]), 2);

            $GLOBALS['outputUSSD']["MSG"] = "Press ok to pop up payment on your MTN MOMO phone.";
            $GLOBALS['outputUSSD']["MSG"] .= "\n An SMS would be sent on successful payment.";
            // $GLOBALS['outputUSSD']["MSG"] .= "\n\n Thank you for using our service.";

           

        } elseif ($momoNetwork == "AIRTEL") {

            $GLOBALS['outputUSSD']["MSG"] = "Press ok to pop up payment on your AIRTEL MONEY phone.";
            $GLOBALS['outputUSSD']["MSG"] .= "\n An SMS would be sent on successful payment.";
            // $GLOBALS['outputUSSD']["MSG"] .= "\n\n Thank you for using our service.";

            

        } elseif ($momoNetwork == "TIGO") {

            $GLOBALS['outputUSSD']["MSG"] = "Press ok to pop up payment on your TIGO CASH phone.";
            $GLOBALS['outputUSSD']["MSG"] .= "\n An SMS would be sent on successful payment.";
            // $GLOBALS['outputUSSD']["MSG"] .= "\n\n Thank you for using our service.";

            
            
        } else {
            $GLOBALS['outputUSSD']["MSG"] .= "An SMS would be sent on successful payment.";
            $GLOBALS['outputUSSD']["MSG"] .= "\n\n Thank you for using our service.";

        }

        $GLOBALS['outputUSSD']["MSGTYPE"] = false;

        createPendingTicketBuyer();

    } elseif ($GLOBALS['inputUSSD']->USERDATA == '0') {

        $GLOBALS['outputUSSD']["MSG"] = "Thank you for using Transport Services.";
        // $GLOBALS['outputUSSD']["MSG"] .= "\n Cheetah, Your #1 student transport services provider.";
        $GLOBALS['outputUSSD']["MSGTYPE"] = false;
    } else {
        wrongInput();
    }
}


function loadCompanies()
{
    $database = json_decode(file_get_contents("vitalInfo"));
  
    $string = "";
    $i = 1;

    foreach ($database->companies as $current) {
        $string .= "\n" . $i . "." . $current[1];
        $i++;
    }

    return $string;
    
}


function loadSchools()
{
    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];
    
    $database = json_decode(file_get_contents("database"))->$selectedCompany;
    
    return drawOptions($database);
}

function loadSchoolsAgent($selectedCompany)
{
    
    $database = json_decode(file_get_contents("database"))->$selectedCompany;
    
    return drawOptions($database);
}

function loadSelectedSchoolDestination()
{
    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];

    $viableDestination = json_decode(file_get_contents("database"))->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]->content;
    
    return drawOptions($viableDestination);

}


function loadSelectedSchoolDestinationDepartureDates()
{   
    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];

    $dates = json_decode(file_get_contents("database"))->$selectedCompany
        [$GLOBALS["sessionData"]["selectedSchool"]]
        ->content[$GLOBALS["sessionData"]["selectedDestination"]]
        ->departure_dates;

    // var_dump($dates);
    $string = "";
    $i = 1;

    $todayDate = new DateTime();

    $datesArray = array();

    foreach ($dates as $key => $departureDate) {
        
        $currentDate = new DateTime($key);

        if ($currentDate > $todayDate) {
            $string .= "\n" . $i . "." . $currentDate->format('d M Y');
            $i++;
            array_push($datesArray, $key);
        }

    }

    $GLOBALS["sessionData"]["availableDates"] = $datesArray;
    return $string;

}

function loadSelectedDateDepartureTime()
{
    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];
    $selectedDate = $GLOBALS["sessionData"]["availableDates"][$GLOBALS["sessionData"]["selectedDepartureDate"]];
        
    $availableTimes = json_decode(file_get_contents("database"))->$selectedCompany
        [$GLOBALS["sessionData"]["selectedSchool"]]
        ->content[$GLOBALS["sessionData"]["selectedDestination"]]
        ->departure_dates
        ->$selectedDate;
    
    $string = "";
    $i = 1;

    
    $timeArray = array();

    foreach ($availableTimes as $departureTime) {
        
        
        $string .= "\n" . $i . "." . $departureTime->time;
        $i++;
        array_push($timeArray, $departureTime);
        
    }

    $GLOBALS["sessionData"]["availableTime"] = $timeArray;
    return $string;

}

function loadHaulageServices()
{
    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];

    $viableServices = json_decode(file_get_contents("database"))
        ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
        ->content[0]
        ->busses;

    $string = "";
    $i = 1;

    foreach ($viableServices as $current) {
        $string .= "\n" . $i . "." . $current->name;
        $string .=":GHS".number_format(
            (($current->amount + $current->momo_charge + $current->nhil)/100),
            2
        );
        $i++;
    }
    
    return $string;
  
}

function loadSelectedSchoolPickupPoints()
{
    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];

    $viablePickupPoints = json_decode(file_get_contents("database"))
        ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
        ->pickupPoints;

    return drawOptions($viablePickupPoints);

}

function loadMOMOOperator()
{
    $momoOption = json_decode(file_get_contents("vitalInfo"))->paymentMethods;

    $string = "";
    $i = 1;

    foreach ($momoOption as $current) {

        if ($current->type == 'momo') {
            $string .= "\n" . $i . "." . $current->name;
            $i++;
        } else {
            if (isset($GLOBALS["sessionData"]["agentId"])) {
                $string .= "\n" . $i . "." . $current->name;
                $i++;
            }
        }
    }

    return $string;
}

function drawOptions($array)
{
    $string = "";
    $i = 1;

    foreach ($array as $current) {
        $string .= "\n" . $i . "." . $current->name;
        $i++;
    }

    return $string;
}




function companySelectedValid()
{
    $availableCompanies = json_decode(file_get_contents("vitalInfo"))->companies;

    if (is_numeric($GLOBALS['inputUSSD']->USERDATA)) {
        
        $selectedIndex = ($GLOBALS['inputUSSD']->USERDATA * 1) - 1;

        if (array_key_exists($selectedIndex, $availableCompanies)) {
            // Pick the company name for the selected company in the USSD
            $GLOBALS["sessionData"]["selectedCompany"] = $availableCompanies[$selectedIndex][0];
            $GLOBALS["sessionData"]["selectedCompanyIndex"] = $selectedIndex;
            return true;
        }
    }

    return false;
    
}


function schoolSelectedValid()
{
    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];
   
    $availableSchools = json_decode(file_get_contents("database"))->$selectedCompany;

    return validateInput($availableSchools, "selectedSchool");
}


function destinationValid()
{   
    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];
   
    $availableDestinations = json_decode(file_get_contents("database"));
    $availableDestinations = $availableDestinations->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]->content;

    return validateInput($availableDestinations, "selectedDestination");
}

function departueDateValid()
{
    $availableDepatureDates = $GLOBALS["sessionData"]["availableDates"];
    return validateInput($availableDepatureDates, "selectedDepartureDate");

}

function pickUpTimeValid()
{
    $availableDepatureTime = $GLOBALS["sessionData"]["availableTime"];
    return validateInput($availableDepatureTime, "selectedDepartureTime");

}


function pickupPointValid()
{
    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];
  
    $viablePicupPoints = json_decode(
        file_get_contents("database")
    )->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
        ->pickupPoints;

    return validateInput($viablePicupPoints, "selectedPickupPoint");
}


function ticketNumberValid()
{
    if (is_numeric($GLOBALS['inputUSSD']->USERDATA)) {

        $GLOBALS["sessionData"]["ticketNo"] = $GLOBALS['inputUSSD']->USERDATA;
        return true;
    }
    return false;
}


function serviceSelectionValid()
{
    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];
  
    $viableServices = json_decode(file_get_contents("database"))
        ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
        ->content[0]
        ->busses;
        // var_dump($viableServices);
        
    if (validateInput($viableServices, "selectedService")) {
        $selectedIndex = ($GLOBALS['inputUSSD']->USERDATA * 1) - 1;
        
        // var_dump($viableServices[$selectedIndex]);
        $GLOBALS["sessionData"]["ticketNo"] = 1;
        $GLOBALS["sessionData"]["serviceName"] = $viableServices[$selectedIndex]->name;
        $GLOBALS["sessionData"]["tickets"] = $viableServices[$selectedIndex]->amount;
        $GLOBALS["sessionData"]["momo_charge"] = $viableServices[$selectedIndex]->momo_charge;
        $GLOBALS["sessionData"]["nhil"] = $viableServices[$selectedIndex]->nhil;
        $GLOBALS["sessionData"]["totalAmount"] = $viableServices[$selectedIndex]->amount
            +$viableServices[$selectedIndex]->momo_charge
            +$viableServices[$selectedIndex]->nhil;
        return true;   
    }
    return false;
    
}


function validMomoNetwork()
{
    $momoOptions = json_decode(file_get_contents("vitalInfo"))->paymentMethods;

    if (is_numeric($GLOBALS['inputUSSD']->USERDATA)) {
        $selectedIndex = ($GLOBALS['inputUSSD']->USERDATA * 1) - 1;

        if (array_key_exists($selectedIndex, $momoOptions)) {
            $currentSelection = $momoOptions[$selectedIndex];

            //used to make sure only agents can make non momo calls
            if ($currentSelection->type != "momo") {
                if (!isset($GLOBALS["sessionData"]["agentId"])) {
                    return false;
                }
                $GLOBALS["sessionData"]["cashPayments"] = true;

            }
            $GLOBALS["sessionData"]["selectedPaymentMethod"] = $selectedIndex;
            return true;
        }
    }

    if ($currentSelection->type == 'momo') {
        $string .= "\n" . $i . "." . $currentSelection->name;
        $i++;
    } else {
        if (isset($GLOBALS["sessionData"]["agentId"])) {
            $string .= "\n" . $i . "." . $currentSelection->name;
            $i++;
        }
    }

    return validateInput($momoOptions, "selectedPaymentMethod");
}

function validVodafoneVoucher()
{
    if (is_numeric($GLOBALS['inputUSSD']->USERDATA) and strlen($GLOBALS['inputUSSD']->USERDATA) == 6) {

        $GLOBALS["sessionData"]["vodafoneVoucher"] = $GLOBALS['inputUSSD']->USERDATA;
        return true;
    }
    return false;
}


function validMSISDN()
{
    $msisdn = isMsisdn($GLOBALS['inputUSSD']->USERDATA);

    if ($msisdn) {
        $GLOBALS["sessionData"]["momoNumber"] = $msisdn;
        return true;
    }
    return false;
}





function validateInput($arraySet, $sessionElementName)
{
    if (is_numeric($GLOBALS['inputUSSD']->USERDATA)) {
        $selectedIndex = ($GLOBALS['inputUSSD']->USERDATA * 1) - 1;

        if (array_key_exists($selectedIndex, $arraySet)) {
            $GLOBALS["sessionData"][$sessionElementName] = $selectedIndex;
            return true;
        }
    }

    return false;
}


function selectAgentCompanyIndex($msisdn)
{
    $companyName = json_decode(file_get_contents("vitalInfo"))->agents->$msisdn->company;
   
    # Since we want to use the same functions as a normal user, selected school indexes would be incremented by 1
    
    $selectedCompanyIndex = 1;
    $availableCompanies = json_decode(file_get_contents("vitalInfo"))->companies;

    foreach ($availableCompanies as $key=>$company) {
        if ($company == $company[0]) {
            $selectedCompanyIndex += $key;
            break;
        }
    }
    return [
        "name"=>$companyName,
        "index" => $selectedCompanyIndex
    ];
}

function wrongInput()
{
    $GLOBALS['outputUSSD']["MSG"] = "Wrong input\n" . $GLOBALS["sessionData"]["previousMessage"];

}




/**
 * Create the invoice for a succesful USSD run.
 * Logging all user details into a database table.
 * 
 * @return NULL
 */
function createPendingTicketBuyer()
{
    $msisdn = $GLOBALS['inputUSSD']->MSISDN;

    if (isset($GLOBALS["sessionData"]["agentId"])) {
        $msisdn = $GLOBALS["sessionData"]["momoNumber"];

    }

    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];
  

    $transport_route_id = json_decode(file_get_contents("database"))
        ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
        ->content[$GLOBALS["sessionData"]["selectedDestination"]]
        ->transport_session_id;

    $transport_bus_id = json_decode(file_get_contents("database"))
        ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
        ->content[$GLOBALS["sessionData"]["selectedDestination"]]
        ->busses[0]->id;

    $paymethod = json_decode(file_get_contents("vitalInfo"))
        ->paymentMethods[$GLOBALS["sessionData"]["selectedPaymentMethod"]]
        ->type;
    
    $invoice_num = uniqid();
    $ticketAmount = $GLOBALS["sessionData"]["tickets"];
    $momo_charge = $GLOBALS["sessionData"]["momo_charge"];;
    $nhil = $GLOBALS["sessionData"]["nhil"];
    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];


    $departureDateId =  $selectedTime =  $GLOBALS["sessionData"]["availableTime"][$GLOBALS["sessionData"]["selectedDepartureTime"]]["id"];
       

    $pickUpPoint = json_decode(file_get_contents("database"))
        ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
        ->pickupPoints[$GLOBALS["sessionData"]["selectedPickupPoint"]]->id;

    if ($paymethod == 'momo') {
        $extraInfo = [
            "momoProvider" => json_decode(file_get_contents("vitalInfo"))
                ->paymentMethods[$GLOBALS["sessionData"]["selectedPaymentMethod"]]
                ->name,
            "momoNumber" => $GLOBALS["sessionData"]["momoNumber"]
        ];



    } else {
        $extraInfo = [
            "agentId" => $GLOBALS["sessionData"]["agentId"]
        ];

    }

    $query = "INSERT INTO invoice 
        (msisdn, transport_route_id, transport_bus_id, number_tickets, `status`, amount, momo_charge, nhil, invoice_no,
         pay_method, extra_info, departure_date_id, pickup_point_id, company)
        VALUES (
            '$msisdn',
            $transport_route_id,
            $transport_bus_id,
            " . $GLOBALS["sessionData"]["ticketNo"] . ",
            'pending',
            $ticketAmount,
            $momo_charge,
            $nhil,
            '" . $invoice_num . "',
            '$paymethod',
            '" . json_encode($extraInfo) . "',
            $departureDateId,
            $pickUpPoint,
            '$selectedCompany'
        )";

    insertIntoDatabase($query);

    if (isset($GLOBALS["sessionData"]["cashPayments"])) {
        
        $url =CASH_MOMO_URL."$invoice_num";


    } else {

        $url = DELAY_MOMO_URL."$invoice_num&msisdn=" . $GLOBALS["inputUSSD"]->MSISDN;
           
    }

    $ch = curl_init();                

        curl_setopt($ch, CURLOPT_URL, $url);
        
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 10);

        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

        
       
        $data = curl_exec($ch);
        // echo $data;

        curl_close($ch);
}



/**
 * Create the invoice for a succesful USSD run.
 * Logging all user details into a database table.
 * 
 * @return NULL
 */
function createHaulageRequester()
{
    $msisdn = $GLOBALS['inputUSSD']->MSISDN;

    if (isset($GLOBALS["sessionData"]["agentId"])) {
        $msisdn = $GLOBALS["sessionData"]["momoNumber"];

    }

    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];
  

    $transport_route_id = json_decode(file_get_contents("database"))
        ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
        ->content[$GLOBALS["sessionData"]["selectedDestination"]]
        ->transport_session_id;

    $transport_bus_id = json_decode(file_get_contents("database"))
        ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
        ->content[$GLOBALS["sessionData"]["selectedDestination"]]
        ->busses[0]->id;

    
    $selectedCompany = $GLOBALS["sessionData"]["selectedCompany"];


    $departureDateId = $GLOBALS["sessionData"]["availableDates"][$GLOBALS["sessionData"]["selectedDepartureDate"]]["id"];

    $pickUpPoint = json_decode(file_get_contents("database"))
        ->$selectedCompany[$GLOBALS["sessionData"]["selectedSchool"]]
        ->pickupPoints[$GLOBALS["sessionData"]["selectedPickupPoint"]]->id;

   

    $query = "INSERT INTO request 
        (msisdn, transport_route_id, transport_bus_id, pickup_point_id, `status`, departure_date_id, company)
        VALUES (
            '$msisdn',
            $transport_route_id,
            $transport_bus_id,
            $pickUpPoint,
            'pending',
            $departureDateId,
            '$selectedCompany'
        )";


    insertIntoDatabase($query);


    $url = HAULER_SMS_URL.$msisdn.
        "&message=".urlencode($GLOBALS["sessionData"]["sendMessage"])
        ."&company=$selectedCompany";
        
        
    $ch = curl_init();                

    curl_setopt($ch, CURLOPT_URL, $url);
    
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 10);

    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);

    
   
    $data = curl_exec($ch);
    echo $data;

    curl_close($ch);
    
}

/**
 * Used to make the connection to the database for insertion
 * 
 * @param String $query MysqlQuery to be fired
 * 
 * @return NULL
 */
function insertIntoDatabase($query)
{
    // Switch between love and localhost

    $dbConnection = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

    if ($dbConnection->connect_errno) {
        echo "Failed to connect to MySQL: " . $dbConnection->connect_error;
    }

    $dbConnection->query($query);

}


function initiateMobileMoney($order_id)
{
    $curl = curl_init();

    $username = MOMO_USERNAME;
    $password = MOMO_PASSWORD;
    $merchantID = MOMO_MERCHANTID;

    $key = rand(1000, 9999);
    $secrete = md5($username . $key . md5($password));

    $momoNetwork = json_decode(file_get_contents("vitalInfo"))
        ->paymentMethods[$GLOBALS["sessionData"]["selectedPaymentMethod"]]
        ->name;

    $momoNumber = $GLOBALS["sessionData"]["momoNumber"];
    $totalAmount = number_format(($GLOBALS["sessionData"]["totalAmount"] / 100), 2);

    if ($momoNetwork == "VODAFONE") {
        $description = $GLOBALS["sessionData"]["vodafoneVoucher"];
    } else {

        $description = "Cheetah_ticket " . date('Y-m-d H:i:s');
    }

    $arrayStructure = [
        "merchant_id" => $merchantID,
        "secrete" => $secrete,
        "key" => $key,
        "order_id" => $order_id,
        "customerName" => "Cheetah Transport",
        "amount" => $totalAmount,
        "item_desc" => $description,
        "customerNumber" => $momoNumber,
        "payby" => $momoNetwork,
        "callback" => MOMO_CALLBACK
    ];

    $input = json_encode($arrayStructure);
    //MTN, AIRTEL, TIGO, VODAFONE
    curl_setopt_array(
        $curl,
        array(
            CURLOPT_URL => "MOMO_API_ENPOINT",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $input,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json"
            ),
        )
    );

    $response = curl_exec($curl);
    $err = curl_error($curl);
    
    curl_close($curl);


    file_put_contents(
        "apiCallLog",
        date('Y-m-d H:i:s') . " [INPUT:$input] [OUTPUT:$response]" . PHP_EOL,
        FILE_APPEND
    );

    $query = "INSERT INTO momo 
        (msisdn, network, amount, `status`, order_id, response, `description`)
        VALUES (
            '$momoNumber',
            '$momoNetwork',
            '$totalAmount',
            'pending',
            '$order_id',
            '$response',
            '$description'
        )";
    insertIntoDatabase($query);

}

?>